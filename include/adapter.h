#pragma once

#include <vector>
#include <iostream>
#include<cstring>
#include <fstream>
#include "transmitter.h"
#include "common_func.h"

using namespace std;
/*
struct msg_info_struct
{
    string topic;
    int32_t partition;
    int64_t offset;
};
*/

class adapter : public component
{
    public:
        adapter() : _trans(NULL) {}
        virtual ~adapter() {}

        //virtual adapter& operator<< (vector<string>& data);
    //    virtual adapter& operator<< (vector<int>& data);

        void bitshift_commit(vector<int>& data, msg_info_struct *msg_info);

        // Transmit buffered data of all channels
        bool transmit();

        // Transmit buffered data of a specific channel
        bool transmit(string chan);

        // Transmit string data
        bool transmit(string chan, const string& data);

        // Transmit a file through a specific channel
        bool transmit(string chan, int infd);
        bool transmit(string chan, int infd, int offset);

        // Commit current state
        bool commit()
        {
        //    return _trans->commit(_doc_id, 1234567);
            return _trans->commit(_doc_id);
        }


        void set_trnasmitter(transmitter* trans)
        {_trans = trans;}

        void set_doc_id(string doc_id)
        {_doc_id = doc_id;}

        int check_flush_time(string condition);
    //    string get_current_date(string format);
    //    void Write_Log(string filename_ending_with, string log_message);
    //    void Write_Error_Log(msg_info_struct *msg_info, string error_message);

    protected:
        // Transform received data from receiver into target format.
        // The result had better be stored in the buffer of transmitter channel.
        // Thus, it should call get_buffer() in advance, and call return_buffer() if not all
        // buffer is consumed.
        // This function also has to call transmit to pass transformed data to next component.
        //virtual bool transform(vector<string>& data, string& doc_id, size_t& count) = 0;
        virtual bool transform(vector<int>& data, string& doc_id, size_t& count, msg_info_struct *msg_info) = 0;

        void* get_buffer(string chan, size_t size)
        {return _trans->get_buffer(chan, size);}

        bool return_buffer(string chan, size_t size)
        {return _trans->return_buffer(chan, size);}

    private:
        transmitter* _trans;

        string _doc_id;    // current document id
};