# Streamer C++

# Change Log
==========================

- 0.0.0 
    - [0.0.1]
        - 新增 flush time: 可讀agent_XXXX.conf裡面的 flush_time_limit來設定最久要幾秒flush一次, (沒寫的話, defualt為300秒)
        - 可從 Go的Parser那邊收到error message並寫進txt的error log, 一天最多一個error log
    - [0.0.2]
        - 加了 bo_transmitter::get_total_count_by_sql(): 為了解決 checkpoint 有時候會被歸 0 的問題, 按照 Edward 的建議, 寫了一個Work around
    - [0.0.3]
        - 加了checkpoint health check: 寫checkpoint之前, 計算實際table的筆數和checkpoint差多少, 異常則寫message log
        - 把多加了 common_func.h 和 common_func.cpp: 把寫log的功能移到外層, 盡量大家都可以隨時call
    - [0.0.4]
        - 修改 kafka_receiver.cpp 裡開啟tempfile的方式: 因為把streamer包進docker執行之後, 碰到file system不支援 open() 裡的 flag: O_TMPFILE, 要另找方法開tempfile
        目前是改用 tmpfile() 來開tempfile, 此方法預設是在 /tmp 裡建 tempfile, close之後就自動砍掉
        也就是說, 從config 讀進來的 _out2file 目前是沒用處了
        - 修改 Write_Log 以及相關 logging 的 function: 多一個parameter, bool print_on_screen, 來決定寫 log 的同時要不要 print 在螢幕上
        - 修改 get_total_count_by_sql: return 改成 bool, 代表exec_sql 是否成功
    - [0.0.5]
        - bo_transmitter.cpp 對 health check 方式有些許改變
        - size_t bo_channel::get_total_count() 不再 retry. 如果 read() 的 error == EINTR, try到成功為止, 否則直接跳出
        - 以 streamer_C: 0.0.4 為基礎新增 influxdb 功能:
            
            - file_receiver 和 kafka_receiver 的 read() 改成 read_receiver() 
                 (start() 的 read() 也改成 read_receiver() )
                 純粹只是想和 system call 的 read() 區分開來, 以免搞混

            
            - 以下多了跟 influxdb 相關的更動:
                - config裡多了 influxdb 的各項參數
                - run_OOXX.sh 裡可多傳一個參數 "influxdb", 有寫的話則寫入influxdb, 沒寫則寫入BO. 也就是兩者擇一
                - Makefile 要加入 csv2influxdb_adapter
                
                - 加了 csv2influxdb_adapter.cpp 和 csv2influxdb_adapter.h
                - sagent.cpp main()
                - bool coordinator::initialize()
                - bool coordinator::sync()
                
            - 以上更動, 重點: 
                - 寫入 influxdb 或 BO, 兩者擇一
                - 一個 influxdb 的 streamer 只負責寫入一個 measurement
                - influxdb 的資料不需 parsing, 所以 data flow 也只到 adapter 為止
                    , 不須經過 transmitter 和 bo_transmitter, 當然也不必經過 JsonToCsv()
                    , 所以在 main() 的時候一旦確定此 streamer 要寫入 influxdb, 就不必宣告和初始化跟 transmitter 相關的所有參數
                - 如果 streamer initialize時讀不到 topic 會 retry
    - [0.0.6]
        - 取消 SUSPEND 和 RESUME, 改用 "flush table OOXX;" 的 sql command
        - 讓所有使用 exec_sql() 都接它回傳的 bool 
        - 因為 influxdb 輸入的資料有可能順序會變來變去, 所以需要經過 JsonToCsv() 將欄位塞到 table 中正確的位置
            修改 influxdb 流程: 從 adpater 改成 transmitter:
            - 刪除 csv2influxdb_adapter.cpp 和 csv2influxdb_adapter.h
            - 新增 influxdb_transmitter.cpp 和 influxdb_transmitter.h
            - 修改 sagent.cpp 裡宣告 object 的方式
            - 修改 Release 裡的 Makefile, 並且 clean 時 rm $(INCDIR)/go_json2csv.h

            - 以上更動, 重點:
                - 寫入 influxdb 或 BO, 兩者擇一
                - 一個 influxdb 的 streamer 可寫入多個 measurement
                - 因為是 transmitter, 所以需要初始化, 也會經過 JsonToCsv()
                - 如果 streamer initialize時讀不到 topic 會 retry
    - [0.0.7]
        - 修改 rdkafka.c 吐 segmentation fault 問題: 在 kafka_receiver::read_receiver() 左右 使用 rd_kafka_topic_name() 之前檢查 pointer 是否為 NULL 
        - influxdb 的 config多了一個 receive_from_begining: true 則 kafka 從第 0 筆開始收, 否則就從 streamer 啟動的時候才開始收
    - [0.0.8]
        - 好像什麼都沒改, 為了交接給 Kevin, 反正就備份