#include <string.h>

#include "bo_counter.h"
#include "coordinator.h"

#define KEY_CORD_GROUP      "syn_group"
#define KEY_CORD_RETENTION  "retention"
#define KEY_CORD_LIFE_TYPE  "life_type"
#define KEY_CORD_START_DAY  "start_day"
#define KEY_CORD_ALOW_ZERO_START    "allow_zerostart"

#define KEY_CORD_COUNTER_IP     "counter.ip"
#define KEY_CORD_COUNTER_PORT   "counter.port"
#define KEY_CORD_COUNTER_DB     "counter.db"
#define KEY_CORD_COUNTER_RETENTION "counter.retention"

bool coordinator::initialize()
{
    if (!_receiver || !_adapter ||  !_transmitter )
        return false;

    ini_conf conf(_conf);
    map<string, string> section;
    string group;
    if (!conf.get_value(SECT_CORD, KEY_CORD_GROUP, group)) {
        Error("%s: missing conf key '%s::%s'\n", __func__, SECT_CORD, KEY_CORD_GROUP);
        return false;
    }
    string retain_days;
    if (conf.get_value(SECT_CORD, KEY_CORD_RETENTION, retain_days)) {
        _retention = stoi(retain_days);
        if (_retention < 1)
            _retention = 1;
    }

    int diff_day = 0;
    time_t start_time = time(NULL);
    struct tm start_utc = {0};
    memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));

    string start_day;
    if (conf.get_value(SECT_CORD, KEY_CORD_START_DAY, start_day)) {
        _start_day = stoi(start_day);
        if (_start_day < 0)
            _start_day = 0;
    }
    string life_type;
    if (conf.get_value(SECT_CORD, KEY_CORD_LIFE_TYPE, life_type)) {
        switch (life_type.at(0)) {
            case 'w':
                _life_type = LIFE_TYPE_WEEKLY;
                diff_day = start_utc.tm_wday - _start_day;
                if (diff_day < 0)
                    diff_day += 7;
                start_time -= diff_day * 86400;
                memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));
                break;
            case 'm':
                _life_type = LIFE_TYPE_MONTHLY;
                if (_start_day == 0)
                    _start_day = 1; // month day is 1 to 31
                if (start_utc.tm_mday < _start_day &&
                    --start_utc.tm_mon < 0) // month is 0 to 11
                    --start_utc.tm_year;
                start_utc.tm_mday = _start_day;
                break;
            case 'f':
                _life_type = LIFE_TYPE_FOREVER;
                memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));
                break;
        }
    }
    update_retention(true);

  
    char date_buf[16] = {0};
    strftime(date_buf, sizeof(date_buf) - 1, "BO%Y%2m%2d", &start_utc);
        
    _transmitter->set_name(date_buf);
    
    

    // set counter
    string counter_ip, counter_port;
    if (conf.get_value(SECT_CORD, KEY_CORD_COUNTER_IP, counter_ip) &&
        conf.get_value(SECT_CORD, KEY_CORD_COUNTER_PORT, counter_port)) 
    {
        unsigned short port = (unsigned short)atoi(counter_port.c_str());
        int retention = 1;
        string counter_retention;
        if (conf.get_value(SECT_CORD, KEY_CORD_COUNTER_RETENTION, counter_retention))
            retention = atoi(counter_retention.c_str());
        if (retention < 1)
            retention = 1;
        bo_counter::get_counter()->set_counter(counter_ip, port, retention);
        // comment out counter to temporary disable counter for kafka consumer
        //_enable_counter = true;
    }

    string zero_start;
    if (conf.get_value(SECT_CORD, KEY_CORD_ALOW_ZERO_START, zero_start))
        _alow_zero_start = zero_start.compare("1") == 0;

    if (!conf.get_section(SECT_RECV, section) ||
        !_receiver->set_params(group, section)) {
        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_RECV);
        return false;
    }

    if (!conf.get_section(SECT_ADPT, section) ||
        !_adapter->set_params(group, section)) {
        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_ADPT);
        return false;
    }

   
        
    if (!conf.get_section(SECT_TRAN, section) ||
        !_transmitter->set_params(group, section)) {

        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_TRAN);
        return false;
    }
    
    _adapter->set_trnasmitter(_transmitter);
    
    _receiver->set_adapter(_adapter);
   
    return true;
}

void* coordinator::counter_worker(void* arg)
{
    bo_counter::get_counter()->start_counter();
}

bool coordinator::run()
{
    pthread_t counter_thr;
    if (_enable_counter) { // ?? 這是幹嘛的 ?
        if (0 != pthread_create(&counter_thr, NULL, counter_worker, NULL)) {
            Error("coordinator::%s, create counter worker fail: %s\n", __func__, strerror(errno));
            _enable_counter = false;
        }
    }

    // $$ 這裡並非while一直跑, 而是進去start之後直到expire為止
    // $$ to receiver::start
    while (_receiver->start(_expire)) { 
        // reset_workspace() will issue back up previous workspace request,
        // so we do it before checking if it's time to switch bo.
        bool change_bo = update_retention(false);

        _transmitter->close_chan();
        _transmitter->reset_workspace(change_bo);
        if (change_bo) {
            // reset next retention time, but not check if expired
            update_retention(true);

            // update bo name
            time_t current_time = time(NULL);
            struct tm current_utc = {0};
            char bo_name[16] = {0};
            memcpy(&current_utc, gmtime(&current_time), sizeof(current_utc));
            strftime(bo_name, sizeof(bo_name) - 1, "BO%Y%2m%2d", &current_utc);
            _transmitter->set_name(bo_name);
        }
        _transmitter->prepare_channels(change_bo);
    }

    if (_enable_counter) {
        bo_counter::get_counter()->stop_counter();

        struct timespec timeout;
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 5;
        if (0 != pthread_timedjoin_np(counter_thr, NULL, &timeout))
            Error("coordinator::%s, join counter thread fail: %s\n", __func__, strerror(errno));
    }

    return true;
}

bool coordinator::sync()
{
//    if(_write_to == "influxdb") // ?? influxdb 目前沒有 checkpoint, 但是要怎麼跟kafka 要求從上次斷掉的地方收? Answer: 其實這樣就已經達成了
  //      return true;

    
    string src, dst, target; // $$ src 和 dst 都是 doc_id
    if(_write_to == "influxdb")
    {
        if(_transmitter->get_receive_from_begining())
        {
            target = ""; 
            if (!_receiver->backoff(target)) // $$ kafka 從第0筆開始收, 否則就是從streamer啟動的時候開始收
                return false;
        }
        return true;
    }
    else
    {
        // 1. get both sides state, 取得各自的 doc_id
        if (!_receiver->get_last_commitment(src) ||
            !_transmitter->get_last_commitment(dst))
            return false;
        
        Error("%s:\nreceiver id: %s\ntransmitter id: %s\n", __func__, src.c_str(), dst.c_str());
        // [YHYU]
        if (dst.length() < 5 && !_alow_zero_start)
        {
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "failed, because doc id is " + dst  + "(length < 5) and allow_zerostart is false", true);
            return false;
        }
        // 2. negotiate
        if (!_receiver->sync_strategy(src, dst, target))  // ?? 還沒看
            return false;

        
        // 3. back off a state that both sides agree if needs
        // $$ _receiver或_transmitter 任何一邊back off return false 則return false. streamer停止
        if (!_receiver->backoff(target) ||
            !_transmitter->backoff(target))
            return false;

        return true;
    }
    return false;
}

/*
bool coordinator::sync()
{
    if(_write_to == "influxdb") // ?? influxdb 目前沒有 checkpoint, 但是要怎麼跟kafka 要求從上次斷掉的地方收? 尚未完成 
        return true;

    
    string src, dst, target; // $$ src 和 dst 都是 doc_id

    // 1. get both sides state, 取得各自的 doc_id
    if (!_receiver->get_last_commitment(src) ||
        !_transmitter->get_last_commitment(dst))
        return false;
    
    Error("%s:\nreceiver id: %s\ntransmitter id: %s\n", __func__, src.c_str(), dst.c_str());
    // [YHYU]
    if (dst.length() < 5 && !_alow_zero_start)
    {
         Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "failed, because doc id is " + dst  + "(length < 5) and allow_zerostart is false", true);
        return false;
    }
    // 2. negotiate
    if (!_receiver->sync_strategy(src, dst, target))  // ?? 還沒看
        return false;

    
    // 3. back off a state that both sides agree if needs
    // $$ _receiver或_transmitter 任何一邊back off return false 則return false. streamer停止
    if (!_receiver->backoff(target) ||
        !_transmitter->backoff(target))
        return false;

    return true;
}
*/


bool coordinator::update_retention(bool init)
{
    time_t now = time(NULL);
    struct tm utc_time = {0};

    memcpy(&utc_time, gmtime(&now), sizeof(utc_time));

    // check if it's time to stop
    if (!init &&
        is_life_end(utc_time)) {
        return true;
    }

    // update next expire time
    time_t diff = (23 - utc_time.tm_hour) * 3600 + (59 - utc_time.tm_min) * 60 + (59 - utc_time.tm_sec);
    // +1: just to be sure it's at the begnning of day, especially negative leap second.
    _expire = now + diff + (_retention - 1) * 86400 + 1;

#ifdef BO_STREAM_AGENT_DEBUG
    char time_buf[64] = {0};
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T %Z", localtime(&_expire));
    Debug("%s, next retention expired time is at %s.\n", __func__, time_buf);
#endif
    Error("%s, next expired time is %lu (now is %lu)\n", __func__, _expire, time(NULL));
    return false;
}

bool coordinator::is_life_end(struct tm& utc_time)
{
    // since we do check at the end/beginning of day, we can just check if
    // it's the date of  _start_day.
    switch (_life_type) {
        case LIFE_TYPE_WEEKLY:
        {
            return (utc_time.tm_wday == _start_day && 0 == utc_time.tm_hour && utc_time.tm_min < 5);
        }
        case LIFE_TYPE_MONTHLY:
        {
            return (utc_time.tm_mday == _start_day && 0 == utc_time.tm_hour && utc_time.tm_min < 5);
        }
        case LIFE_TYPE_FOREVER:
        {
            return false;
        }
    }
    return false;
}
