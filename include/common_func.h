#pragma once

#include <string.h>

#include <fstream>

#include <iostream>

using namespace std;



struct msg_info_struct
{
    string topic;
    int32_t partition;
    int64_t offset;
};



void Write_Log(string filename_ending_with, string log_message, bool print_on_screen);
void Write_Parse_ErrorLog(msg_info_struct *msg_info, string error_message, bool print_on_screen);    
void Write_System_ErrorLog(string file_name, int line, string function_name, string error_message, bool print_on_screen);



void Write_StressTestingLog(string error_message, bool print_on_screen);
// void Write_ConnectionLog(int fd, string action, string file_name, int line, string function_name, string other_desc);


string get_current_date(string format);

