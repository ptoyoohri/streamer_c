#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <sstream>

#include "bo_counter.h"
#include "kafka_receiver.h"

#include <iostream>
using namespace std;

#define KEY_RECV_KAFKA_BROKER   "broker"
#define KEY_RECV_KAFKA_TOPIC    "topics"
#define KEY_RECV_KAFKA_GROUP    "kafka_group"
#define KEY_RECV_KAFKA_CODEC    "codec"
#define KEY_RECV_KAFKA_OUT2FILE "out2file"
#define KEY_RECV_KAFKA_SYNCTYPE "sync_type"
#define KEY_RECV_KAFKA_READTIMEOUT  "read_timeout"
#define KEY_RECV_KAFKA_BATCH_READ   "batch_read"
#define KEY_RECV_KAFKA_MSG_TAILING  "msg_tailing"
#define KEY_RECV_KAFKA_MSG_HEADING  "msg_heading"
#define KEY_RECV_KAFKA_MSG_APPEND_TIME  "msg_append_time"


kafka_receiver::~kafka_receiver()
{
    //if (_kafka_conf)
    //    rd_kafka_conf_destroy(_kafka_conf);
    if (_kafka) {
        rd_kafka_resp_err_t err = rd_kafka_consumer_close(_kafka);
        if (err)
            Error("%s, Failed to close consumer: %s\n", __func__, rd_kafka_err2str(err));

        rd_kafka_destroy(_kafka);
    }
}

bool kafka_receiver::set_params(string& group, map<string, string>& param)
{
    if (!receiver::set_params(group, param))
        return false;

    string batch;
    if (get_param_value(KEY_RECV_KAFKA_BATCH_READ, batch))
        _batch_read = stoi(batch);
    if (_batch_read < 1)
        _batch_read = 1;

    string tailing, heading, appending;
    if (get_param_value(KEY_RECV_KAFKA_MSG_TAILING, tailing))
        _msg_tailing = tailing.compare("1") == 0;
    if (get_param_value(KEY_RECV_KAFKA_MSG_HEADING, heading))
        _msg_heading = heading.compare("1") == 0;
    if (get_param_value(KEY_RECV_KAFKA_MSG_APPEND_TIME, appending))
        _msg_appending = appending.compare("1") == 0;

    get_param_value(KEY_RECV_KAFKA_BROKER, _broker);
    get_param_value(KEY_RECV_KAFKA_GROUP, _kafka_group);

    string all_topics;
    get_param_value(KEY_RECV_KAFKA_TOPIC, all_topics);

    string::size_type pos = 0;
    string::size_type next_pos = 0;
    while (string::npos != (next_pos = all_topics.find_first_of(',', pos))) {
        _topics.push_back(all_topics.substr(pos, next_pos - pos));
        pos = next_pos + 1;
    }
    if (pos < all_topics.length())
        _topics.push_back(all_topics.substr(pos));

    if (_broker.empty() || _topics.empty() || _kafka_group.empty())
        return false;

	string read_timeout;
	if (get_param_value(KEY_RECV_KAFKA_READTIMEOUT, read_timeout))
		_read_timeout = stoi(read_timeout);
    
	string sync_type;
	if (get_param_value(KEY_RECV_KAFKA_SYNCTYPE, sync_type) && !sync_type.empty()) {
        switch (sync_type.at(0)) {
            case 'L':
                _stype = KRECV_SYNCTYPE_RECV_LAST;
                break;
            case 'R':
                _stype = KRECV_SYNCTYPE_RECV;
                break;
            case 'T':
                _stype = KRECV_SYNCTYPE_TRANS;
                break;
            case 'N':
            default:
                _stype = KRECV_SYNCTYPE_NORM;
                break;
        }
    }

    struct stat outdir_stat = {0};
    get_param_value(KEY_RECV_KAFKA_OUT2FILE, _out2file);
    if (!_out2file.empty()) {
        if (0 != stat(_out2file.c_str(), &outdir_stat)) {
            if (0 != mkdir(_out2file.c_str(), S_IRWXU)) {
                Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
                return false;
            }
        }
    }

    char err_buf[1024] = {0};
    rd_kafka_topic_conf_t* topic_conf = rd_kafka_topic_conf_new();
    if (rd_kafka_topic_conf_set(topic_conf, "offset.store.method", "broker",
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    _kafka_conf = rd_kafka_conf_new();
    if (rd_kafka_conf_set(_kafka_conf, "enable.auto.commit", "true",
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }
    //if (rd_kafka_conf_set(_kafka_conf, "group.protocol.type", "consumer",
    //        err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
    //    Error("%s, %s\n", __func__, err_buf);
    //    return false;
    //}
    if (rd_kafka_conf_set(_kafka_conf, "group.id", _kafka_group.c_str(),
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }
    rd_kafka_conf_set_default_topic_conf(_kafka_conf, topic_conf);

    // optional
    string codec;
    get_param_value(KEY_RECV_KAFKA_CODEC, codec);
    if (!codec.empty()) {
        if (rd_kafka_conf_set(_kafka_conf, "compression.codec", codec.c_str(),
                err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
            Error("%s, %s\n", __func__, err_buf);
            return false;
        }
    }
    rd_kafka_conf_set_opaque(_kafka_conf, this);
    rd_kafka_conf_set_rebalance_cb(_kafka_conf, rebalance_cb);

    _kafka = rd_kafka_new(RD_KAFKA_CONSUMER, _kafka_conf, err_buf, sizeof(err_buf)); // $$ to librdkafka line 1487
    if (!_kafka || 0 == rd_kafka_brokers_add(_kafka, _broker.c_str())) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    // get current stored offsets ?? 怎麼做的?
    if (!get_partition_offset(_offsets))
        return false;

    // redirect rd_kafka_poll to consumer poll
    rd_kafka_poll_set_consumer(_kafka);

    // subscribe we interested topics and all partitions
    int tidx = 0;
    rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(_topics.size());
    for (tidx = 0; tidx < _topics.size(); ++tidx)
        rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), -1);
    rd_kafka_resp_err_t err = rd_kafka_subscribe(_kafka, topic_list);
    if (err)
        Error("%s, Fail to subscribe topic: %s\n", __func__, rd_kafka_err2str(err));
    rd_kafka_topic_partition_list_destroy(topic_list);

    return err ? false : true;
}

int kafka_receiver::find_topic_index(const char* topic)
{
    int idx = 0;
    for (; idx < _topics.size(); ++idx) {
        if (_topics[idx].compare(topic) == 0)
            return idx;
    }
    return -1;
}


static void get_msg_info(rd_kafka_message_t *rkmessage, msg_info_struct *msg_info)
{
    if(rkmessage)
    {
        if(rkmessage->rkt)
            msg_info->topic = rd_kafka_topic_name(rkmessage->rkt);
    
        msg_info->partition = rkmessage->partition;
        msg_info->offset = rkmessage->offset;
    }
}

ssize_t kafka_receiver::read_receiver(string& data, int& fd, msg_info_struct *msg_info)
{
    rd_kafka_resp_err_t err = RD_KAFKA_RESP_ERR_NO_ERROR;
    rd_kafka_message_t* msg = NULL;
    int msg_count = 0;
    fd = -1;
    
    do 
    {
    
		data.clear();
        err = RD_KAFKA_RESP_ERR_NO_ERROR;
        

        // $$ 傳入_kafka, 會把裡面的跟kafka相關的資訊傳給 msg
        msg = rd_kafka_consumer_poll(_kafka, _read_timeout); // <-- when msg is 0, means time out, flush

    
        if (msg)  // msg is not 0
        {
        //    Write_StressTestingLog("kafka received first data", true);
            err = msg->err;
            
            if (err == RD_KAFKA_RESP_ERR_NO_ERROR) 
            {
                static bool first_time = true;
                if(first_time)
                {
                    first_time = false;
                    Write_StressTestingLog("first time", true);
                }
                
                
                get_msg_info(msg, msg_info); // $$ output: msg_info

            
                // $$ kafka 曾經發生過 segmentation fault, 所以多加了這個防止 rkt is null
                if (!msg->rkt)
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__)
                        , string("rkt is null")
                        + ", offset: " + to_string(msg->offset) 
                        + ", " + to_string(msg->len) +  " bytes)", true);
                    rd_kafka_message_destroy(msg);
                    continue;
                }
                // update offset
                int tidx = find_topic_index(rd_kafka_topic_name(msg->rkt));
                if (-1 == tidx) {
                    Error("%s, can't find topic '%s' index.\n", __func__, rd_kafka_topic_name(msg->rkt));
                    rd_kafka_message_destroy(msg);
                    continue;
                }
                _offsets[tidx][msg->partition] = msg->offset;

                int64_t enter_microsec = 0;
                if (_msg_appending) {
                    struct timeval cur_time = {0};
                    gettimeofday(&cur_time, NULL);
                    enter_microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                }

                string doc_id;
                get_last_commitment(doc_id);

                if (!_out2file.empty()) 
                { // $$ /tmp/kafka_bo存在
                
                    //ostringstream outpath;
                    //outpath << _out2file << "/" << doc_id;
                    //
                    //ofstream outfile(outpath.str());
                    //if (outfile.is_open()) {
                    //    outfile.write((const char*)msg->payload, (streamsize)msg->len);
                    //    outfile.close();
                    //    
                    //    // copy output file path
                    //    data = outpath.str();
                    //}
                    //else
                    //    Error("%s, open output file '%s' fail: %s\n", __func__, outpath.str().c_str(), strerror(errno));
                
                    FILE *filepointer = NULL;

                    if (fd > 0) {
                        if (write(fd, "\n", 1) <= 0) {
                            rd_kafka_message_destroy(msg);
                            Error("%s, write kafka message separator fail: %s\n", __func__, strerror(errno));
                            break;
                        }
                    }
                    else
                    {
                        /*
                            $$
                            原本是 fd = open(_out2file.c_str(), O_RDWR|O_TMPFILE|O_EXCL|O_CLOEXEC , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);

                            為了克服streamer在docker化遭遇到的困難: 
                            O_RDWR 導致 EISDIR: Is a directory
                            O_TMPFILE 導致 EOPNOTSUPP:Operation not supported

                            因為container裡面的file system是比較新的OverlayFS, 外面的linux 是比較舊的ext4, 也就是新的系統反而不支援用open開tempfile:
                            因為目前原因是出在O_TMPFILE在OverlayFS不支援, 但刪掉O_TMPFILE又會因為O_RDWR所以無法開啟folder (也就是說 O_RDWR和O_TMPFILE同時使用才能達到把folder當成tempfile的效果)

                            解法:
                            其實有另一個方式可以開啟暫存檔, tmpfile(), 會return 一個file pointer, 再用 fileno() 它來取得 fd
                        */

                       // $$ 用 tmpfile() 這個來取代 原本的temp file, 這樣就不需要 _out2file 了?
                       // $$ 在這邊開啟一個temp file, 把資料寫進去, 在 json2csv_adapter::transform()裡傳給 Golang 的 JsonToCsv() 之後 close(files_fd[i])
                        filepointer = tmpfile(); 

                        fd = fileno(filepointer);
                    }
                    if (-1 == fd)
                        Error("%s, open temp file for kafka message fail: %s\n", __func__, strerror(errno));
                    else {
                        bool has_error = false;
                        if (_msg_heading) {
                            string heading(string(rd_kafka_topic_name(msg->rkt)) + ",");
                            if (write(fd, heading.c_str(), heading.length()) <= 0) {
                                Error("%s, write kafka message header (%s) fail: %s\n", __func__, heading.c_str(), strerror(errno));
                                has_error = true;
                            }
                        
                        }
                        if (!has_error) {
                            if (_msg_appending) {
                                char* payload = new char[msg->len + 1];
                                memcpy(payload, msg->payload, msg->len);
                                payload[msg->len] = 0;
                                char* token = strtok(payload, "\n");
                                char rec[1024] = {0};
                                while (token) {
                                    // char* rec = new char[strlen(token) + 128];
                                    struct timeval cur_time = {0};
                                    gettimeofday(&cur_time, NULL);

                                    int64_t microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                                    sprintf(rec, "%s,%ld,%ld\n", token, enter_microsec, microsec);
                                    if (write(fd, (const char*)rec, strlen(rec)) <= 0) {
                                        Error("%s, write appendind timestamp fail: %s\n", __func__, strerror(errno));
                                        has_error = true;
                                    }
                                    // delete [] rec;
                                    token = strtok(NULL, "\n");
                                }
                                delete [] payload;
                            }
                            else if (write(fd, (const char*)msg->payload, (size_t)msg->len) <= 0) {
                                Error("%s, write kafka message fail: %s\n", __func__, strerror(errno));
                                has_error = true;
                            }
                            //else if (_msg_appending) {
                            //    struct timeval cur_time = {0};
                            //    gettimeofday(&cur_time, NULL);
                            //
                            //    char buf[128] = {0};
                            //    int64_t microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                            //    sprintf(buf, ",%ld", microsec);
                            //    if (write(fd, (const char*)buf, strlen(buf)) <= 0) {
                            //        Error("%s, write appendind timestamp fail: %s\n", __func__, strerror(errno));
                            //        has_error = true;
                            //    }
                            //}
                        }

                        if (has_error) {
                            if (msg_count > 0) {
                                rd_kafka_message_destroy(msg);
                                break;
                            }

                        //    Write_ConnectionLog(fd, "close", string(__FILE__), __LINE__, string(__func__), "");
                            close(fd);
                            
                            fd = -1;
                        }
                        else {
                            ++msg_count;
                            if (_msg_tailing && ((const char*)msg->payload)[msg->len - 1] != '\n' &&
                                msg_count >= _batch_read) { // this is the last msg in a batch
                                if (write(fd, "\n", 1) <= 0)
                                    Error("%s, write kafka message tail fail: %s\n", __func__, strerror(errno));
                            }
                        }

                    }
                    
                    if (-1 == fd) {
                        rd_kafka_message_destroy(msg);
                        continue;
                    }
                }
                else
                {
                    // copy data
                    // $$ 成功從kafka上收到資料(msg->payload), copy給data
                    // $$ 而fd是從write取得的 (write kafka的 header, appending, or message)
                    data = (const char*)msg->payload;
                //    Debug("%s, received message (topic %s [%d], offset %ld, %lu bytes): %s\n",
                //        __func__, rd_kafka_topic_name(msg->rkt), msg->partition, msg->offset, msg->len, (const char*)msg->payload);
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__)
                        , string("received message (topic: ") + rd_kafka_topic_name(msg->rkt) 
                        + "[" + to_string(msg->partition) + "]" 
                        + ", offset: " + to_string(msg->offset) 
                        + ", " + to_string(msg->len) +  " bytes): " + data, true);

                }

                _adapt->set_doc_id(doc_id.c_str());
                bo_counter::get_counter()->inc_val(BCOUNT_KAFKA);
            }
            else
            {
                Debug("%s, partition %d, offset %ld, err: %s\n", __func__, msg->partition, msg->offset, rd_kafka_err2str(err));
            //    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "patiotion: " + to_string(msg->partition) + ", offset: " + to_string(msg->offset) + ", err: " + rd_kafka_err2str(err), true);
            }
            rd_kafka_message_destroy(msg);
        }
    // !msg means read timeout, just leave it to have caller to handle timeout
    //} while (!msg || err == RD_KAFKA_RESP_ERR__PARTITION_EOF);
    
    } while (err == RD_KAFKA_RESP_ERR__PARTITION_EOF || (msg && msg_count < _batch_read));

    
    // return 1 for timeout, b/c 0 means failure
    //return data.empty() ? 1 : data.length();
    return 1;
}

bool kafka_receiver::get_last_commitment(string& doc_id)
{
    // $$ kafka 是用 doc_id 來記錄目前送到哪 (offsets)
    bool is_empty = true;
    ostringstream last_commit;
    int i = 0;
    for (; i < _topics.size(); ++i) { // $$ 跑過每個 topic
        last_commit << _topics[i];

        vector<ssize_t>::const_iterator off = _offsets[i].begin();
        for (; off != _offsets[i].end(); ++off) { // $$ 該 topic 底下的 partition
            last_commit << "_" << *off;
            is_empty &= (*off == RD_KAFKA_OFFSET_INVALID);
        }

        // separater of topic
        if (i <  (_topics.size() - 1))
            last_commit << "=";
    }

    if (is_empty)
        doc_id.clear();
    else
        doc_id = last_commit.str();
    Debug("kafka_receiver::%s: last commitment is '%s'\n",
        __func__, doc_id.empty() ? "NONE" : doc_id.c_str());
    return true;
}

bool kafka_receiver::backoff(string& doc_id)
{
    // $$ 透過 doc_id 取得 bo_裡記錄的 kafka offset, 然後用 set_partition_offset() 寫進 kafka
    string last_doc;
    get_last_commitment(last_doc);

    // no change
    if (!last_doc.empty() && last_doc.compare(doc_id) == 0)
        return true;

    vector<vector<ssize_t> > org_offsets = _offsets;
    if (doc_id.empty()) {
        vector<vector<ssize_t> >::iterator off = _offsets.begin();
        for (; off != _offsets.end(); ++off)
            off->assign(off->size(), RD_KAFKA_OFFSET_INVALID);
    }
    else if (!get_partition_offset_from_docid(doc_id, _offsets))
        return false;
    else if (_offsets.size() != org_offsets.size() || _topics.size() != _offsets.size()) {
        Error("%s, number of topics is not match\n", __func__);
        return false;
    }
    else {
        int tidx = 0;
        for (; tidx < _topics.size(); ++tidx) {
            if (_offsets[tidx].size() < org_offsets[tidx].size())
                _offsets[tidx].resize(org_offsets[tidx].size(), RD_KAFKA_OFFSET_INVALID);
        }
    }

    return set_partition_offset();
}

bool kafka_receiver::sync_strategy(string& rdoc, string& tdoc, string& out)
{
    switch (_stype) {
        case KRECV_SYNCTYPE_RECV:
        case KRECV_SYNCTYPE_RECV_LAST:
            out = rdoc;
            return true;
        case KRECV_SYNCTYPE_TRANS:
            out = tdoc;
            return true;
    }

    if (rdoc.empty() || tdoc.empty()) {
        out.clear();
        return true;
    }

    if (rdoc.compare(tdoc) == 0 ) {
        out = rdoc;
        return true;
    }

    vector<vector<ssize_t> > part_offset1, part_offset2;
    if (!get_partition_offset_from_docid(rdoc, part_offset1))
        return false;
    if (!get_partition_offset_from_docid(tdoc, part_offset2))
        return false;
    if (part_offset1.size() != part_offset2.size() || _topics.size() != part_offset1.size()) {
        Error("%s, number of topics is not match.\n", __func__);
        return false;
    }

    int tidx = 0;
    out.clear();
    for (; tidx < _topics.size(); ++tidx) {
        vector<ssize_t>::size_type part_cnt1 = part_offset1[tidx].size();
        vector<ssize_t>::size_type part_cnt2 = part_offset2[tidx].size();
        if (part_cnt1 > part_cnt2)
            out = tdoc;
        else if (part_cnt1 < part_cnt2)
            out = rdoc;
        else {
            vector<ssize_t>::const_iterator it1 = part_offset1[tidx].begin();
            vector<ssize_t>::const_iterator it2 = part_offset2[tidx].begin();
            for (; it1 != part_offset1[tidx].end(); ++it1, ++it2) {
                if (*it1 == *it2)
                    continue;
                out = *it1 > *it2 ? tdoc : rdoc;
                break;
            }
        }
        if (!out.empty())
            break;
    }
    return true;
}

bool kafka_receiver::get_partition_offset_from_docid(const string& doc_id, vector<vector<ssize_t> >& part_offset)
{
    if (doc_id.empty())
        return false;

    part_offset.clear();
    part_offset.reserve(_topics.size());

    int tidx = 0;
    string::size_type pos = 0, next_pos = 0;
    string sub_id = doc_id;
    for (tidx = 0; tidx < _topics.size(); ++tidx) {
        if (sub_id.length() <= (_topics[tidx].length() + 1) ||
            sub_id.compare(0, _topics[tidx].length() + 1, _topics[tidx] + "_") != 0)
            break;

        vector<ssize_t> offsets;
        pos = _topics[tidx].length() + 1;
        while (string::npos != (next_pos = sub_id.find_first_of("_=", pos))) {
            offsets.push_back(stoll(sub_id.substr(pos, next_pos - pos)));
            if (sub_id.at(next_pos) == '=')
                break; // end of topic
            pos = next_pos + 1;
        }

        if (string::npos == next_pos) {
            offsets.push_back(stoll(sub_id.substr(pos)));
            part_offset.push_back(offsets);
            ++tidx;
            break;
        }

        part_offset.push_back(offsets);
        sub_id = sub_id.substr(next_pos + 1);
    }
    if (tidx != _topics.size()) {
        part_offset.clear();
        return false;
    }
    return true;
}

bool kafka_receiver::get_partition_offset(vector<vector<ssize_t> >& part_offset)
{
    // ?? 怎麼做到的 ?
    int tidx = 0;
    part_offset.clear();
    part_offset.reserve(_topics.size());

    for (; tidx < _topics.size(); ++tidx) {
        vector<ssize_t> offsets;
        const struct rd_kafka_metadata *metadata;
        rd_kafka_topic_t* rkt = rd_kafka_topic_new(_kafka, _topics[tidx].c_str(), rd_kafka_topic_conf_new());
        if (!rkt) {
            Error("%s, Fail to create kafka topic object.\n", __func__);
            return false;
        }

        rd_kafka_resp_err_t err = rd_kafka_metadata(_kafka, 0, rkt, &metadata, 5000);
        if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
            Error("%s, Fail to get meta data: %s\n", __func__, rd_kafka_err2str(err));
            rd_kafka_topic_destroy(rkt);
            return false;
        }

        if (metadata->topic_cnt != 1 || metadata->topics[0].err) {
            Error("%s, Number of topic (%d) mismatched, or with error: %s\n",
                __func__, metadata->topic_cnt, rd_kafka_err2str(metadata->topics[0].err));
            rd_kafka_topic_destroy(rkt);
            return false;
        }

        int part_cnt = metadata->topics[0].partition_cnt;
        rd_kafka_metadata_destroy(metadata);
        offsets.reserve(part_cnt);

        int i = 0;
        if (_stype == KRECV_SYNCTYPE_RECV_LAST) {
            for (i = 0; i < part_cnt; ++i) {
                int64_t low = 0, high = 0;
                err = rd_kafka_query_watermark_offsets(_kafka, _topics[tidx].c_str(), i, &low, &high, 5000);
                if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
                    Error("%s, get water mark of %s[%d] fail: %s\n",
                        __func__, _topics[tidx].c_str(), i, rd_kafka_err2str(err));
                    return false;
                }
                offsets.push_back(high == 0 ? RD_KAFKA_OFFSET_INVALID : high - 1);
            }
            part_offset.push_back(offsets);
            continue;
        }

        // prepare partition list
        rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(part_cnt);
        for (i = 0; i < part_cnt; ++i)
            rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), i);

        err = rd_kafka_committed(_kafka, topic_list, 5000);

        if (err == RD_KAFKA_RESP_ERR_NO_ERROR) {
            for (i = 0; i < topic_list->cnt ; i++) {
                rd_kafka_topic_partition_t *p = &topic_list->elems[i];
                if (p->err) {
                    Error("%s, partition %d error: %s\n", __func__, i, rd_kafka_err2str(p->err));
                    return false;
                }
                offsets.push_back(p->offset);
                Debug("%s, partition %d offset: %ld\n", __func__, i, p->offset);
            }
        }
        rd_kafka_topic_partition_list_destroy(topic_list);
        part_offset.push_back(offsets);
    }

    return true;
}

bool kafka_receiver::set_partition_offset()
{
    // ?? 弄半天之後 destory 掉, 這個 function 難道只是看 offset 有沒有錯而已嗎?

    if (_offsets.empty())
        return false;

    int tidx = 0, pidx = 0, part_cnt = 0;
    for (tidx = 0; tidx < _offsets.size(); ++tidx) // $$ 跑過每個 topic, 把他們的 partition 加總
        part_cnt += _offsets[tidx].size();

    // $$ rd_kafka_topic_partition_list_new: 建立一個 topic + partition 的 vector/list, part_cnt 相當於 size
    rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(part_cnt);

    rd_kafka_resp_err_t err = RD_KAFKA_RESP_ERR_NO_ERROR;
    for (tidx = 0; tidx < _offsets.size(); ++tidx) { // $$ 跑過每個 topic
        for (pidx = 0; pidx < _offsets[tidx].size(); ++pidx) { // $$ 跑過該 topic 底下的每個 partition

            // $$ 把 topic + partition 加到 vector 裡面
            rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), pidx);

            // $$ _offsets[topic][partition]
            // ?? 這邊把 _offsets 抓出來 +1 要幹嘛
            int64_t offset = _offsets[tidx][pidx] == RD_KAFKA_OFFSET_INVALID ? 0 : _offsets[tidx][pidx] + 1;

            // $$ 把 offset 存到 某 topic + partition 裡
            // $$ offset 可以是一個數值, 或以下其中一種: RD_KAFKA_OFFSET_BEGINNING, RD_KAFKA_OFFSET_END, RD_KAFKA_OFFSET_STORED, RD_KAFKA_OFFSET_TAIL
            err = rd_kafka_topic_partition_list_set_offset(topic_list, _topics[tidx].c_str(), pidx, offset);
            if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
                Error("%s, set topic %s, partition %d, offet %ld fail: %s\n", __func__, _topics[tidx].c_str(), pidx, offset, rd_kafka_err2str(err));
                break;
            }
            Debug("%s, set topic %s, partition %d, offset: %ld, success\n", __func__, _topics[tidx].c_str(), pidx, offset);
        }
    }
    if (err == RD_KAFKA_RESP_ERR_NO_ERROR) {
        // $$ 把剛剛組好的 topic_list, assign 給 _kafka 
        err = rd_kafka_assign(_kafka, topic_list);
        if (err != RD_KAFKA_RESP_ERR_NO_ERROR)
            Error("%s, assign partition offsets fail: %s\n", __func__, rd_kafka_err2str(err));
    }

    rd_kafka_topic_partition_list_destroy(topic_list);
    return err == RD_KAFKA_RESP_ERR_NO_ERROR;
}

void kafka_receiver::rebalance_cb(
    rd_kafka_t *kafka,
    rd_kafka_resp_err_t err,
    rd_kafka_topic_partition_list_t *partitions,
    void *arg)
{
	if (err == RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS) {
		Debug("%s, assigned\n", __func__);
        // reassign offset w/ our offset
        kafka_receiver* krecv = (kafka_receiver*)arg;
        krecv->set_partition_offset();
    }
    else if (err == RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS) {
		Debug("%s, revoked\n", __func__);
        rd_kafka_assign(kafka, NULL);
    }
    else {
		Error("%s, rebalance fail: %s\n", __func__, rd_kafka_err2str(err));
        rd_kafka_assign(kafka, NULL);
		return;
	}
}