#pragma once

#include "transmitter.h"
#include "common_func.h"

typedef struct bo_replica
{
    string host;
    unsigned short port;
    bool no_sync;
} bo_replica_t;

typedef struct bo_commit_act
{
    string source;
    string host;
    unsigned short port;
} bo_commit_act_t;

typedef enum {
    SVC_BO,
    SVC_REPLICA,
    SVC_BACKUP
} sa_svc_type; // service type

class bo_channel : public channel
{
    public:
        bo_channel() : _sql(""), _is_dim(false), _fd(-1) {}
        bo_channel(string name, size_t cap) :
            channel(name, cap), _sql(""), _is_dim(false), _fd(-1) {}
        bo_channel(string name, size_t cap, string sql, bool dim) :
            channel(name, cap), _sql(sql), _is_dim(dim), _fd(-1) {}
        virtual ~bo_channel();

        bool create_table(string ws);
        void set_fd(int fd);

        const int get_fd() const
        {return _fd;}

        bool close_chan(bool force);

        virtual bool get_total_count(size_t &total_count);
        virtual ssize_t transmit(void* data, size_t size);
        virtual ssize_t transmit(int infd);
        virtual ssize_t transmit(int infd, int offset);

        bool is_dim_tbl() const
        {return _is_dim;}

        static bool exec_sql(int fd, string ws, string sql, bool csv_out, string& result);
        static bool pre_transmit(int fd, string table, string type, ssize_t shrink);
        static bool send_csv_header(int fd, string table, ssize_t shrink);
    
        virtual void StressTesting(size_t total_count);

        
    private:
        string _sql;
        bool _is_dim;
        int _fd;
};

class bo_transmitter : public transmitter
{
    public:
        bo_transmitter() : _host(""), _port(0), _bk_port(0), _type(""),
        _workspace(""), _fix_ws(""), _pre_create(false), _create_cp(false), _cp_name(""),
        _cp_retention(604800), _fd_cp(-1), _allow_cleanup(false), _replica_num(0),
        _commit_act(false), _no_suspend(false)
        {}
        virtual ~bo_transmitter();

        virtual bool get_last_commitment(string& doc_id);
        virtual bool backoff(string& doc_id);

        virtual bool set_params(string& group, map<string, string>& param);
        virtual bool add_chan(string chan_name, size_t size);

        virtual void reset_workspace(bool change_bo);
        virtual bool prepare_channels(bool create_cp);

        // overload the function to add special handling before and after commit
        virtual bool commit(string& doc_id);
        
        virtual bool flush(string& chan_name, int infd);
        virtual bool flush(string& chan_name, int infd, int offset);
        
        virtual bool reset_chan(channel* chan);

        virtual void close_chan(channel* chan);
        virtual bool get_total_count_by_sql(string workspace, string table_name, size_t &total_count);
        virtual const bool get_receive_from_begining() // $$ for influxdb
        {return false;}
        virtual const string get_class_name()
        {return "bo_transmitter";}
    protected:
        virtual bool set_checkpoint(commitment& cp);
        bool get_checkpoint(string table, string doc_id, string& ws, ssize_t& count);
    //    bool allow_backoff;
    private:
        
        string _host;
        unsigned short _port;
        unsigned short _bk_port; // backup service port
        string _type;
        string _workspace; // $$ workspace for data table, defined in conf "workspace"
        string _fix_ws;
        bool _pre_create;
        bool _create_cp;
        string _cp_name;
        int _cp_retention;
        int _fd_cp; // for check point

    

        int _replica_num;
        bo_replica_t _replica;
        bool _allow_cleanup;
        bool _commit_act;
        bo_commit_act_t _cmact;

        bool _no_suspend;

        int open_connection(uint64_t read_timeout, sa_svc_type svc_type);
        int simple_connection(const string& peer, unsigned short port);
        bool initialize();
        bool create_workspace();

        // suspend and resume bo to make sure bo data is flush to disk.
        bool flush_bo();
        bool flush_bo(sa_svc_type svc_type);

        bool flush_cmd(sa_svc_type svc_type);

        bool reset_sync_chan();

        void send_commit_action();
        
};