#pragma once

#include <map>
#include <string>

#include <time.h>
#include "common_func.h"

#include "component.h"

#define CHAN_BUF_SIZE_DEFAULT   16*1024*1024
#define EXIT_CODE_STOP_AT_FAIL  101

class channel
{
    public:
        channel() : _name(""),
        _capacity(CHAN_BUF_SIZE_DEFAULT), _size(CHAN_BUF_SIZE_DEFAULT),
        _head(0), _tail(0), _data(NULL), _progress(0),
        _is_replica(false), _sync_replica(false)
        {}
        channel(string name, size_t cap);
        virtual ~channel();

        bool is_empty()
        {return _head == _tail;}

        // buffer can't across end point, b/c we need a continuous buffer, 
        bool is_enough(size_t size)
        {return _head >= _tail ? _capacity > (_head + size) : _tail > (_head + size);}

        void* acquire(size_t size) {
            char* out = NULL;
            if (_head < _tail) {
                if (_tail <= (_head + size))
                    return NULL;
                out = _data + _head;
                _head += size;
                return out;
            }
            if (_capacity > (_head + size)) {
                out = _data + _head;
                _head += size;
            }
            else if (_tail > size) {
                _size = _head;
                _head = size;
                out = _data;
            }
            return (void*)out;
        }

        void* get_data(size_t& size) {
            if (is_empty())
                return NULL;

            char* out = _data + _tail;
            size_t max_size = _head >= _tail ? _head - _tail : _size - _tail;
            if (size > max_size || size == 0)
                size = max_size;
            // Don't move data pointer until release() is called explicitly, b/c data is in use.
            //_tail += size;
            //_tail %= _size;
            return (void*)out;
        }

        // release used data buffer
        void release(size_t size)
        {_tail = (_tail + size) % _capacity;}

        // withdraw over-acquired buffer (assume it follows acquire())
        void withdraw(size_t size) {
            _head -= size;
            if (0 == _head) { // It only happens when returns all previously acquired and across boundary
                _head = _size;
                _size = _capacity;
            }
        }

        const string get_name() const
        {return _name;}

        virtual const int get_fd() const = 0;

        void reset_progress()
        {_progress = 0;}
        void inc_progress(size_t progress)
        {_progress += progress;}
        const size_t get_progress() const
        {return _progress;}

        bool flush();
        bool flush(int infd);
        bool flush(int infd, int offset);
        virtual bool get_total_count(size_t &total_count) = 0;

        bool is_replica() const
        {return _is_replica;}
        void set_replica(bool set)
        {_is_replica = set;}

        bool is_sync_replica() const
        {return _sync_replica;}
        void set_sync_replica(bool set)
        {_sync_replica = set;}

        virtual void StressTesting(size_t total_count) = 0;
     
    protected:
        virtual ssize_t transmit(void* data, size_t size) = 0;
        virtual ssize_t transmit(int infd) = 0; // transmit the whole file data
        virtual ssize_t transmit(int infd, int offset) = 0; // transmit the whole file data start from offset

    private:
        string _name;

        size_t _capacity;   // total reserved size
        size_t _size;       // used size <= _capacity
        size_t _head;
        size_t _tail;

        char* _data;        // this is a mmap buffer(?)

        size_t _progress;       // number of records are processed

        bool _is_replica;
        bool _sync_replica;
};

class transmitter : public component
{
    public:
        transmitter() : _name(""), _stop_at_fail(false)
        {_channels.clear();}
        virtual ~transmitter();

        // Synchronize current state
        // Parameters:
        //      doc_id: last committed doc
        // Return value: success or not
        virtual bool get_last_commitment(string& doc_id) = 0;

        // Back to old position
        // Parameters:
        //      backto: back to the committed doc
        // Return value: success or not
        virtual bool backoff(string& doc_id) = 0;

        virtual bool add_chan(string chan_name, size_t size) = 0;

        void* get_buffer(string chan_name, size_t size) {
            channel* chan = get_chan(chan_name);
            if (!chan)
                return NULL;
            return chan->acquire(size);
        }

        bool return_buffer(string chan_name, size_t size) {
            channel* chan = get_chan(chan_name);
            if (!chan)
                return false;
            chan->release(size);
            return true;
        }

        //transmitter& operator<< (seq_buf* buf);
        // Flush _buf to destination
        // Return value:
        //      = 0: Success
        //      < 0: Fail
        virtual bool flush(string& chan_name);
        virtual bool flush(channel* chan);
        virtual bool flush(string& chan_name, int infd);
        virtual bool flush(string& chan_name, int infd, int offset);
        virtual bool flush(channel* chan, int infd);
        virtual bool flush(channel* chan, int infd, int offset);
        virtual bool flushall();

        // Commit all channels

        virtual bool commit(string& doc_id);

        // Commit a specific channel
        virtual bool commit(string& chan_name, string& doc_id);
        virtual bool commit(channel* chan, string& doc_id);

        void reset_progress();
        void reset_progress(string& chan_name);
        void reset_progress(channel* chan)
        {chan->reset_progress();}

        void inc_progress(string& chan_name, size_t progress);
        void inc_progress(channel* chan, size_t progress)
        {chan->inc_progress(progress);}

        size_t get_progress(string& chan_name);
        size_t get_progress(channel* chan)
        {return chan->get_progress();}

        virtual bool del_chan(string chan_name);

        virtual bool reset_chan(string chan_name);
        virtual bool reset_chan(channel* chan){return true;}

        virtual void close_chan(channel* chan) = 0;
        virtual void close_chan();

        channel* get_chan(string chan_name) {
            map<string, channel*>::const_iterator it = _channels.find(chan_name);
            if (it != _channels.end())
                return it->second;
            return NULL;
        }

        virtual bool set_params(string& group, map<string, string>& param);

        virtual void reset_workspace(bool change_bo){}
        virtual bool prepare_channels(bool create_cp){}

        void set_name(const string& name)
        {_name = name;}
        const string& get_name()
        {return _name;}
        virtual const bool get_receive_from_begining() = 0; // $$ for influxdb
        virtual const string get_class_name()
        {return "transmitter";}
    protected:
        // Write commitment signature (check point).
        // The signature should contain document id, and number of success/fail rows in the document.
        // Return value: success or not
        virtual bool set_checkpoint(commitment& cp) = 0;
        size_t get_main_channel_size();

        string  _name;
        bool    _stop_at_fail;
        map<string, channel*>    _channels;
};