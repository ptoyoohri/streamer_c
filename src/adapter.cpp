#include <string.h>

#include "adapter.h"

bool adapter::transmit()
{
    return _trans->flushall();
}

bool adapter::transmit(string chan)
{
    return _trans->flush(chan);
}

bool adapter::transmit(string chan, const string& data)
{
    void* buf = get_buffer(chan, data.length());
    if (!buf) {
        Error("%s: No enough buffer for data size %lu\n", __func__, data.length());
        return false;
    }
    memcpy(buf, data.c_str(), data.length());
    return transmit(chan);
}

bool adapter::transmit(string chan, int infd)
{
    return _trans->flush(chan, infd);
}

bool adapter::transmit(string chan, int infd, int offset)
{
    return _trans->flush(chan, infd, offset);
}




/* 

adapter& adapter::operator<< (vector<int>& data)
{
    string doc_id;
    size_t count = 0;
    if (transform(data, doc_id, count) && !doc_id.empty())
        _doc_id = doc_id;
    return *this;
}
*/

void adapter::bitshift_commit(vector<int>& data, msg_info_struct *msg_info)
{
    // $$ 此function用來取代之前的覆寫運算子 <<, 因為運算子 << 似乎無法攜帶兩個 parameter
    string doc_id;
    size_t count = 0;
    
    // $$ 在 set_adapter 就定義了要去哪一個 class 的 transform
    bool transform_success = transform(data, doc_id, count, msg_info);
    if(!transform_success)
    {
        Write_Parse_ErrorLog(msg_info, string(__func__)  + ", transform failed: " + strerror(errno), true);
    }

    bool doc_id_empty = doc_id.empty();
    if (transform_success && !doc_id_empty) // $$ 理論上 如果走 influxdb 根本不會有 doc_id(因為沒有checkpoint), 所以也進不去這個 if
        _doc_id = doc_id;
}

 
int adapter::check_flush_time(string condition)
{
    static int last_flush_time = 0;
    int time_difference = 0;
    int current_time = stoi(get_current_date("unix"));

    if(condition == "commit")
            last_flush_time = current_time;
    else if(condition == "compare")
    {
        if(last_flush_time == 0)
            last_flush_time = current_time; // $$ has never been flush before, set last_flush_time now;
        else
            time_difference = current_time - last_flush_time;
    }

    return time_difference;
}

