#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sstream>

#include "file_receiver.h"

#include <iostream>
using namespace std;

#define KEY_RECV_FILE_PATH      "data_path"
#define KEY_RECV_FILE_PREFIX    "prefix"
#define KEY_RECV_FILE_INTERVAL  "interval"
#define KEY_RECV_FILE_SYNCTYPE  "sync_type"

bool file_receiver::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string interval;
    if (get_param_value(KEY_RECV_FILE_INTERVAL, interval))
        _interval = stoul(interval);
    get_param_value(KEY_RECV_FILE_PATH, _path);
    get_param_value(KEY_RECV_FILE_PREFIX, _prefix);

	string sync_type;
	if (get_param_value(KEY_RECV_FILE_SYNCTYPE, sync_type) && !sync_type.empty()) {
        switch (sync_type.at(0)) {
            case 'L':
                _stype = FRECV_SYNCTYPE_RECV_LAST;
                break;
            case 'R':
                _stype = FRECV_SYNCTYPE_RECV;
                break;
            case 'T':
                _stype = FRECV_SYNCTYPE_TRANS;
                break;
            case 'N':
            default:
                _stype = FRECV_SYNCTYPE_NORM;
                break;
        }
    }

    return true;
}

ssize_t file_receiver::read_receiver(string& data, int& fd, msg_info_struct *msg_info)
{

    static bool retry_timeout = false;
    ostringstream file_path;
    ostringstream file_base_name;
    file_base_name << _prefix << "_" << _file_id;
    file_path << _path << "/" << file_base_name.str();

    if (!retry_timeout) // avoid printing out same message constantly
        Error("%s: waiting for next doc '%s'\n", __func__, file_path.str().c_str());

    int retry = 10;
    while (-1 == (fd = open(file_path.str().c_str(), O_RDONLY|O_CLOEXEC))) {
        if (--retry <= 0) {
            retry_timeout = true;
            break;
        }

        sleep(_interval);
    }

    if (-1 == fd) // might read timeout
        return 1;

    retry_timeout = false;

    _adapt->set_doc_id(file_base_name.str());

    ++_file_id;
    //sleep(_interval);
    return 1;
}

bool file_receiver::get_last_commitment(string& doc_id)
{
    if (_file_id <= 0)
        _file_id = 1;

    while (true) {
        ostringstream file_path;
        file_path << _path << "/" << _prefix << "_" << _file_id - 1;
        if (-1 == access(file_path.str().c_str(), F_OK)) {
            --_file_id;
            break;
        }
        ++_file_id;
    }
    if (_file_id > 0) {
        ostringstream file_base_name;
        file_base_name << _prefix << "_" << _file_id - 1;
        doc_id = file_base_name.str();
    }
    Debug("%s: last commitment is '%s'\n", __func__, doc_id.empty() ? "NONE" : doc_id.c_str());
    return true;
}

bool file_receiver::backoff(string& doc_id)
{
    if (doc_id.empty()) {
        _file_id = 0;
        return true;
    }

    string file_base = _prefix + "_";
    if (doc_id.length() <= file_base.length() ||
        0 != doc_id.substr(0, file_base.length()).compare(file_base))
        return false;
    _file_id = stoul(doc_id.substr(file_base.length())) + 1;
    return true;
}

bool file_receiver::sync_strategy(string& rdoc, string& tdoc, string& out)
{
    switch (_stype) {
        case FRECV_SYNCTYPE_RECV:
        case FRECV_SYNCTYPE_RECV_LAST:
            out = rdoc;
            return true;
        case FRECV_SYNCTYPE_TRANS:
            out = tdoc;
            return true;
    }

    if (rdoc.empty() || tdoc.empty()) {
        out.clear();
        return true;
    }

    out = rdoc.compare(tdoc) > 0 ? tdoc : rdoc;
    return true;
}