#pragma once

#include "adapter.h"
#include "common_func.h"

typedef enum
{
    AST_STOP,
    AST_START
} agent_state_t;

class receiver : public component
{
    public:
        receiver() : _adapt(NULL), _state(AST_STOP), _batch_process(1)
        {}
        virtual ~receiver() {}

        virtual bool set_params(string& group, map<string, string>& param);

        virtual ssize_t read_receiver(string& data, int& fd, msg_info_struct *msg_info) = 0;

        // Synchronize current state
        // Parameters:
        //      doc_id: last committed doc
        // Return value: success or not
        virtual bool get_last_commitment(string& doc_id) = 0;

        // Back to old position
        // Parameters:
        //      doc_id: back to the committed doc
        // Return value: success or not
        virtual bool backoff(string& doc_id) = 0;

        // Determine how to synchronize two check points, doc1 and doc2.
        virtual bool sync_strategy(string& rdoc, string& tdoc, string& out) = 0;

        bool start(time_t retention);
        void stop();

        void set_adapter(adapter* adapt)
        {_adapt = adapt;}

        void set_state(agent_state_t state)
        {_state = state;}

        bool is_running()
        {return _state == AST_START;}

        bool is_expired(time_t expire);
    protected:
        adapter* _adapt;

    private:
        agent_state_t   _state;
        int             _batch_process;
};