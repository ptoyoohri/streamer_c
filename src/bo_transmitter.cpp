#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <strings.h>
#include <string.h>
#include <errno.h>

#include <sstream>

//#include "bo_counter.h"
#include "bo_transmitter.h"
#include "bo_stream.h"

#include <iostream>

using namespace std;

#define KEY_TRAN_BO_HOST    "host"
#define KEY_TRAN_BO_PORT    "port"
#define KEY_TRAN_BO_BKPORT  "bk_port"
#define KEY_TRAN_BO_TYPE    "type"
#define KEY_TRAN_BO_SQL     "sql"
#define KEY_TRAN_BO_DIM_TBL "dimension"
#define KEY_TRAN_BO_PRECREATE   "pre_create"
#define KEY_TRAN_BO_CREATECP    "create_cp"
#define KEY_TRAN_BO_CPNAME      "cp_name"
#define KEY_TRAN_BO_CPRETENTION "cp_retention"
#define KEY_TRAN_BO_REPLICA     "replica"
#define KEY_TRAN_BO_NO_SYNC     "no_sync"
#define KEY_TRAN_BO_FIX_WS      "workspace"
#define KEY_TRAN_BO_REPLICA_ONLY    "replica_only"
#define KEY_TRAN_BO_ALLOW_CLEANUP   "alow_cleanup"
#define KEY_TRAN_BO_COMMIT_ACTION   "commit_action"
#define KEY_TRAN_BO_SOURCE          "source"
#define KEY_TRAN_BO_NO_SUSPEND      "no_suspend"

#define TRAN_BO_READ_TIMEOUT    3

//////////////////////////////////////////////////
// bo_channel
//////////////////////////////////////////////////
bo_channel::~bo_channel()
{
    if (-1 != _fd) {
    //    Write_ConnectionLog(_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd);
        _fd = -1;
    }
}

bool bo_channel::create_table(string ws)
{
    if (-1 == _fd)
        return false;

    string dummy;
    bool ret = exec_sql(_fd, _is_dim ? "" : ws, _sql, false, dummy);

//    Write_ConnectionLog(_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(_fd);

    _fd = -1;
    return ret;
}

void bo_channel::set_fd(int fd)
{
    if (-1 != _fd)
    {
//        Write_ConnectionLog(_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd);
    }
    _fd = fd;
}

bool bo_channel::close_chan(bool force)
{
    if (-1 == _fd)
        return true;

    if (!is_empty())
        flush();
    if (!is_empty() && !force)
        return false;

//    Write_ConnectionLog(_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(_fd);

    _fd = -1;
}

ssize_t bo_channel::transmit(void* data, size_t size)
{

    if (-1 == _fd)
        return 0;

    return ::send(_fd, data, size, MSG_DONTWAIT); // ?? 這邊的用意 ? 9091 如何處理 ?
}

ssize_t bo_channel::transmit(int infd)
{
    return transmit(infd, 0);
}

ssize_t bo_channel::transmit(int infd, int offset)
{
    if (-1 == _fd || -1 == infd)
        return 0;

    off_t off_begin = offset; // ?? size_t is for objects, off_t is for files.
    struct stat in_stat = {0};
    if (0 != fstat(infd, &in_stat)) 
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__),  "fstat of fd " + to_string(infd) + " failed, " + " because of : " + strerror(errno), true);
        return 0;
    }
    // avoid send empty file fail and retry
    if (in_stat.st_size == 0)
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__),  "avoid send empty file fail and retry", true);
        return 1;
    }
    /* 
        $$ _fd: table 的 fd
        $$ infd: csv 的 fd
        $$ off_begin: 從哪裡開始 ?? 我看傳進來都是 0 ? Answer: 廢話, 傳一個csv過去, 當然是從頭開始送
        $$ in_stat.st_size: csv 的 size
        看 document 為什麼csv要送這種格式
    */
    ssize_t send_len = sendfile(_fd, infd, &off_begin, in_stat.st_size); 
//    cout<<"send_len: "<<infd<<": "<<send_len<<endl;
    //if (send_len > 0)
    //    bo_counter::get_counter()->inc_val(BCOUNT_TRANS);
    
    return send_len;
}


bool bo_channel::get_total_count(size_t &total_count)
{
    // $$ 如果出錯, 我在這邊不reset, 而是回到 transmitter 那邊再用現成的 function reset
    bool success = false;

    if (-1 == _fd)
        return false;

    uint64_t bt_size = 0;
    
    int ret = -1;
    // $$ syn_bt_size_mark 裡面裝的 S 代表可以得到table size
    ret = ::write(_fd, syn_bt_size_mark, sizeof(syn_bt_size_mark));
    if(ret >= 0)
        ret = read(_fd, &bt_size, sizeof(bt_size));
    
    if(ret >= 0)
    {
        total_count = bt_size;
        success = true;
    }
    else
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("read or write failed: ") + strerror(errno) + ", fd " + to_string(_fd), true);
    
    return success;
    
}


void bo_channel::StressTesting(size_t total_count)
{
    static bool first_time = true;
//    size_t static temp; 

    size_t static last_total_count;
    size_t static total_received;

    if(first_time)
    {
        first_time = false;
        last_total_count = total_count;
        total_received = 0;
//        temp = 0;
    }
    else
    {
        size_t received = total_count - last_total_count;
        if(received <= 0)
        {
            Write_StressTestingLog(to_string(total_count) + "<=" + to_string(last_total_count), true);
        }
        else 
        {
//            size_t interval = 10000;
            total_received = total_received + received;

//            temp = temp + received;
//            if(temp >= interval)
//            {
//                temp = 0;
                Write_StressTestingLog("total received: " + to_string(total_received), true);
//            }

            last_total_count = total_count;
        }
    }
   
}

bool bo_channel::pre_transmit(int fd, string table, string type, ssize_t shrink)
{
    // $$ 根據document, 如果此 fd 是 csv 的話就先送一個 csv header + SYN 'T'
    // $$ 其他資料就透過此fd來寫 (flush 或 一般寫資料)

    if (-1 == fd)
        return false;

    Debug("%s: prepare data stream header, table '%s', type '%s', shrink %ld (fd %d)\n",
        __func__, table.c_str(), type.c_str(), shrink, fd);
    if (type.compare("csv") == 0) {
        if (!bo_channel::send_csv_header(fd, table, shrink))
            return false;
    }
    return true;
}

bool bo_channel::send_csv_header(int fd, string table, ssize_t shrink)
{
    // $$ 讀 document
    // $$ send header
    const uint8_t csv_header[4] = {'c', 's', 'v', 1}; // $$ <-- 1 代表 version 1
    if (4 != ::send(fd, csv_header, 4, 0)) {
        Error("%s: send csv tag fail (%s)\n", __func__, strerror(errno));
        return false;
    }

    // $$ send table name, header 之後一定要立刻接 table name 
    if (table.length() != ::send(fd, table.c_str(), table.length(), 0)) {
        Error("%s: send csv table name fail (%s)\n", __func__, strerror(errno));
        return false;
    }

    // $$ 其他 如果要加 parameter, 則用 '\0' 分開每個 parameter, 並用 '\n' 結尾
    char csv_param[64] = {0};
    ssize_t param_len = 1;
    if (shrink >= 0) {
        csv_param[0] = '\0';
        param_len += snprintf(csv_param + 1, sizeof(csv_param) - 1, "shrink_db=%ld\n", shrink);
    }
    else
        csv_param[0] = '\n';

    if (param_len != ::send(fd, csv_param, param_len, 0)) {
        Error("%s: send csv parameter fail (%s)\n", __func__, strerror(errno));
        return false;
    }

    // $$ 最後補上一個 terminate SYN 信號
    if (SYN_MARK_LEN != ::write(fd, syn_term_mark, SYN_MARK_LEN))
        return false;
    return true;
}

bool bo_channel::exec_sql(int fd, string ws, string sql, bool csv_out, string& result)
{
    //  $$ {'i', 'n', 't', 1} + ws.table + {size + cmd + flag + flag_ex}} + sql command + SYN 'T'

    // send internal command
    // $$ {'i', 'n', 't', 1}
    if (sizeof(int_tag) != ::write(fd, int_tag, sizeof(int_tag))) // $$ int_tag 是 protocal 的 header, 用這個header決定這次連線要幹嘛
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "send internal command error", true);
        return false;
    }
    // send dummy table name
    string tbl_name =  ws + ".";
    if ((tbl_name.length() + 1) != ::write(fd, tbl_name.c_str(), tbl_name.length() + 1))
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "send dummy table name error", true);
        return false;
    }
    // send pull command
    bo_int_header_t int_header = {0};
    int_header.size = sizeof(bo_int_header_t) + sql.length() + 1;
    int_header.cmd = BO_INT_CMD_QUERY; // $$ 這是 QUERY command
    if (csv_out)
        int_header.flags_ex = BO_INT_FLAG_EX_CSV;
    else // only support csv output, otherwise no output
        int_header.flags_ex = BO_INT_FLAG_EX_NO_RET;


    // $$ For ad-hoc QUERY command, query string is after 8 bytes internal stream header. 
    // $$ {size + cmd + flag + flag_ex}
    if (sizeof(int_header) != ::write(fd, &int_header, sizeof(int_header)))
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "send internal header error", true);
        return false;
    }
    // send sql command
    if (::write(fd, sql.c_str(), sql.length() + 1) != (sql.length() + 1))
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "send sql command error: " + sql, true);
        return false;
    }
    // write end tag to avoid blocking
    if (SYN_MARK_LEN != ::write(fd, syn_term_mark, SYN_MARK_LEN))
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "write end tag error", true);
        return false;
    }
    Debug("%s: %s\n", __func__, sql.c_str());

    // wait for job done
    uint8_t dummy[1024] = {0};
    ssize_t r_size = 0;
    bool start_read = true;
    ostringstream out;
    while(0 < (r_size = ::read(fd, dummy, sizeof(dummy) - 1))) {
        dummy[r_size] = 0;
        // check error
        // $$ start_read 控制只有第一次迴圈如何跑
        if (start_read && r_size > (sizeof(int_tag) + sizeof(bo_int_header_t) + sizeof(int64_t)) &&
            0 == memcmp(dummy, int_tag, sizeof(int_tag))) {
            size_t dummy_tbl_len = strlen((const char*)&dummy[sizeof(int_tag)]);
            bo_int_header_t *int_hdr = (bo_int_header_t*)&dummy[sizeof(int_tag) + dummy_tbl_len + 1];
            if (int_hdr->cmd == BO_INT_CMD_REPORT) {
                int64_t* errcode = (int64_t*)((int8_t*)int_hdr + sizeof(bo_int_header_t));
                char* errmsg = (char*)((char*)errcode + sizeof(int64_t));
                Error("%s, ws: '%s', sql: '%s', errcode: %ld, errmsg: '%s'\n",
                    __func__, ws.c_str(), sql.c_str(), *errcode, errmsg);
                if(*errcode != -6)
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "ws: " + ws + ", sql: " + sql + ", errcode: " + to_string(*errcode) + ", errmsg: " + string(errmsg), true);
                }
                return *errcode == -6 ? true : false; // -6 BOEEXIST
            }
        }
        start_read = false;

        if (csv_out)
            out << dummy;
    }
    if (csv_out) {
        result = out.str();
        // trim last '\n'
        if (!result.empty() && result.at(result.length() - 1) == '\n')
            result = result.substr(0, result.length() - 1);
    }
    return true;
}

//////////////////////////////////////////////////
// bo_transmitter
//////////////////////////////////////////////////
bo_transmitter::~bo_transmitter()
{
    if (_fd_cp != -1) {
//        Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd_cp);
        _fd_cp = -1;
    }
}

bool bo_transmitter::initialize()
{
//    allow_backoff = true;

    if (!get_param_value(KEY_TRAN_BO_HOST, _host)) {
        Error("%s: missing conf key '%s'\n", __func__, KEY_TRAN_BO_HOST);
        return false;
    }
    string port;
    if (!get_param_value(KEY_TRAN_BO_PORT, port)) {
        Error("%s: missing conf key '%s'\n", __func__, KEY_TRAN_BO_PORT);
        return false;
    }
    _port = (unsigned short)atoi(port.c_str());

    string bkport;
    if (get_param_value(KEY_TRAN_BO_BKPORT, bkport))
        _bk_port = (unsigned short)atoi(bkport.c_str());

    if (!get_param_value(KEY_TRAN_BO_TYPE, _type)) {
        Error("%s: missing conf key '%s'\n", __func__, KEY_TRAN_BO_TYPE);
        return false;
    }
    
    reset_workspace(false);

    // create all channels' table as well as data connection
    if (_pre_create) {
        if (!prepare_channels(true))
            return false;
    }

    return true;
}

// $$ 看 http://docs.bigobject.io/Data_Import/Streaming_Bulk_Import.html
int bo_transmitter::open_connection(uint64_t read_timeout, sa_svc_type svc_type)
{
    int fd = -1;
    bool is_replica = (svc_type == SVC_REPLICA);
    const char* peer = is_replica ? _replica.host.c_str() : _host.c_str();
    struct hostent *host;
    struct sockaddr_in addr;

    if ((host = gethostbyname(peer)) == NULL)
        return -1;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return -1;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(is_replica ? _replica.port : (svc_type == SVC_BACKUP ? _bk_port : _port));
    addr.sin_addr.s_addr = *(long*)(host->h_addr);

    /* $$ 
        SVC_BO,
        SVC_REPLICA,
        SVC_BACKUP
    */
   /*
    if(read_timeout > 0 )
        other_desc = string(", ") + string("read time out") + string(", ") + other_desc;

    if(svc_type == SVC_BO)
        other_desc = string(", ") + string("SVC_BO") + string(", ") + other_desc;
    if(svc_type == SVC_REPLICA)
        other_desc = string(", ") + string("SVC_REPLICA") + string(", ") + other_desc;
    if(svc_type == SVC_BACKUP)
        other_desc = string(", ") + string("SVC_BACKUP") + string(", ") + other_desc;
*/
//    Write_ConnectionLog(fd, "open", file_name, line, function_name, other_desc);
    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {

    //    Write_ConnectionLog(fd, "close", file_name, line, function_name, other_desc);
        close(fd);

        Error("Connect fail: %s\n", strerror(errno));
        return -1;
    }

    if (read_timeout > 0) { // ?? 為什麼有些要設成 timeout 有些不用? 
        struct timeval timeout = {.tv_sec = (time_t)read_timeout};
        if (-1 == setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const void*)&timeout, sizeof(timeout))) {

        //    Write_ConnectionLog(fd, "close", file_name, line, function_name, other_desc);
            close(fd);

            Error("set socket receive timeout fail: %s\n", strerror(errno));
            return -1;
        }

        if (is_replica && _replica.no_sync) { // $$ 如果是 replica 就設成 non-blocking (否則都是 blocking)
            int flags = fcntl(fd, F_GETFL, 0);
            if (flags == -1) {
                Error("%s, fcntl F_GETFL fail: %s\n", __func__, strerror(errno));
                return fd;
            }

            flags |= O_NONBLOCK;
            if (fcntl(fd, F_SETFL, flags) == -1) {
                Error("%s, set non-blocking fail: %s\n", __func__, strerror(errno));
            }
        }
    }

    return fd;
}

int bo_transmitter::simple_connection(const string& peer, unsigned short port)
{
    int fd = -1;
    struct hostent *host;
    struct sockaddr_in addr;

    if ((host = gethostbyname(peer.c_str())) == NULL)
        return -1;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return -1;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr);

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
    //    Write_ConnectionLog(fd, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(fd);
        Error("Connect fail: %s\n", strerror(errno));
        return -1;
    }

    return fd;
}

bool bo_transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string pre_create;
    if (get_param_value(KEY_TRAN_BO_PRECREATE, pre_create))
        _pre_create = pre_create.compare("1") == 0;
    string create_cp;
    if (get_param_value(KEY_TRAN_BO_CREATECP, create_cp))
        _create_cp = create_cp.compare("1") == 0;
    if (!get_param_value(KEY_TRAN_BO_CPNAME, _cp_name))
        return false;

    string cp_retention;
    if (get_param_value(KEY_TRAN_BO_CPRETENTION, cp_retention)) {
        _cp_retention = atoi(cp_retention.c_str());
    }

    string allow_cleanup;
    if (get_param_value(KEY_TRAN_BO_ALLOW_CLEANUP, allow_cleanup))
        _allow_cleanup = allow_cleanup.compare("1") == 0;

    // replica has to be set before add channels
    string rep_num;
    if (get_param_value(KEY_TRAN_BO_REPLICA, rep_num))
        _replica_num = atoi(rep_num.c_str());
    if (_replica_num) {
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_HOST, _replica.host)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_HOST);
            return false;
        }
        string rep_port;
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_PORT, rep_port)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_PORT);
            return false;
        }
        _replica.port = (unsigned short)atoi(rep_port.c_str());

        string rep_nosync;
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_NO_SYNC, rep_nosync)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_NO_SYNC);
            return false;
        }
        _replica.no_sync = rep_nosync.compare("1") == 0;
    }

    string commit_action;
    if (get_param_value(KEY_TRAN_BO_COMMIT_ACTION, commit_action))
        _commit_act = commit_action.compare("1") == 0;
    if (_commit_act) {
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_SOURCE, _cmact.source)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_SOURCE);
            return false;
        }
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_HOST, _cmact.host)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_HOST);
            return false;
        }
        string cmact_port;
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_PORT, cmact_port)) {
            Error("%s: missing conf key '%s.%s'\n", __func__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_PORT);
            return false;
        }
        _cmact.port = (unsigned short)atoi(cmact_port.c_str());
    }

    string no_suspend;
    if (get_param_value(KEY_TRAN_BO_NO_SUSPEND, no_suspend))
        _no_suspend = no_suspend.compare("1") == 0;

    get_param_value(KEY_TRAN_BO_FIX_WS, _fix_ws);

    if (!transmitter::set_params(group, param))
        return false;

    return initialize();
}

bool bo_transmitter::add_chan(string chan_name, size_t size)
{
    // $$ 從 config 裡讀 table name, 一個一個建出 fd 加到 _channels 裡
    channel* chan = get_chan(chan_name);
    if (chan)
        return false;

    string is_replica_only;
    ostringstream ro_key;
    ro_key << chan_name << "." << KEY_TRAN_BO_REPLICA_ONLY;
    get_param_value(ro_key.str(), is_replica_only);

    string sql;
    if (is_replica_only.compare("1")) {
        //if (_pre_create) {
            ostringstream sql_key;
            sql_key << chan_name << "." << KEY_TRAN_BO_SQL;
            if (!get_param_value(sql_key.str(), sql)) {
                Error("%s: missing conf key '%s'\n", __func__, sql_key.str().c_str());
                return false;
            }
        //}
        string is_dim;
        ostringstream dim_key;
        dim_key << chan_name << "." << KEY_TRAN_BO_DIM_TBL;
        get_param_value(dim_key.str(), is_dim);

        _channels.insert(pair<string, channel*>(chan_name, new bo_channel(chan_name, size, sql, is_dim.compare("1") ? false : true)));
        Debug("Added channel: %s (size %lu)\n", chan_name.c_str(), size);
    }

    if (_replica_num) {
        ostringstream rep_sql_key;
        rep_sql_key << chan_name << "." << KEY_TRAN_BO_REPLICA << "." << KEY_TRAN_BO_SQL;
        if (!get_param_value(rep_sql_key.str(), sql)) {
            Error("%s: no replica for '%s'\n", __func__, rep_sql_key.str().c_str());
            // return false;
            return true;
        }
        bo_channel* chan_rep = new bo_channel(chan_name, size, sql, true);
        chan_rep->set_replica(true);
        if (!_replica.no_sync)
            chan_rep->set_sync_replica(true);
        _channels.insert(pair<string, channel*>(chan_name + "._replica_", chan_rep));
        Debug("Added channel: %s._replica_ (size %lu)\n", chan_name.c_str(), size);
    }
    
    return true;
}

bool bo_transmitter::get_total_count_by_sql(string workspace, string table_name, size_t &total_count)
{
    bool success = false;

    int fd_count = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO);
    string result;

    if (-1 == fd_count)
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "fd_total_count failed", true);
    else 
    {
        ostringstream sql;
        sql << "SELECT count(*) FROM " << table_name;
        // $$ bool bo_channel::exec_sql(int fd, string ws, string sql, bool csv_out, string& result)
        success = bo_channel::exec_sql(fd_count, workspace, sql.str(), true, result); // $$ bool csv_out, 寫 true 的話 result 才能拿到東西
        if(success)
        {
            // $$ turn string to size_t
            std::stringstream string_to_size_t(result); 
            string_to_size_t >> total_count;
        }
        else
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__),  "exec_sql fail, result: " + result + "; " + strerror(errno), true);

        close(fd_count);
    }

    return success;
}

bool bo_transmitter::set_checkpoint(commitment& cp)
{

    
    if (-1 == _fd_cp)
        return false;

    //===============Work around below==============================
    // $$ cp.get_total_count() 得到的結果是在 flush 之後, 順便帶一個 bt_size 的 tag 回來
    // $$ 目前猜測 checkpoint 被歸0有可能是 BO 出了某些問題, 所以帶回來的 bt_size 是 0
    // $$ Edward給的建議, 如果 cp.get_total_count() 拿到的 total_count 為 0, 就用 sql 請 BO 實際 count(*) 一次
    size_t total_count = cp.get_total_count();
//  
    if (total_count == 0) 
    {
        size_t total_count_by_sql = 0;
        bool success;
        success = get_total_count_by_sql(_workspace, cp.get_table_name(), total_count_by_sql);
        
        if(success)
            total_count = total_count_by_sql;
        else
        {
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("get_total_count_by_sql: ") + strerror(errno), true);
            return false;
        }
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), to_string(success) + " original total_count: 0, work around " + _workspace + "." + cp.get_table_name() + ": " + to_string(total_count), true);
    }
    //===============Work around above===============================
    char time_buf[64] = {0};
    ostringstream data;
    struct timeval time_stamp = cp.get_time_stamp();
    data << _group << "," << _workspace << "," << cp.get_table_name() << ",";
    data << cp.get_doc_id() << "," << cp.get_success_count() << "," << total_count /*cp.get_total_count()*/ << ",";
    data << time_stamp.tv_sec * 1000 + time_stamp.tv_usec/1000 << ",";
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&time_stamp.tv_sec));
    data << time_buf << "\n";

    ssize_t write_len;
    
    int retry = 5;
//    int try_after = 1;
    size_t data_len = data.str().length();

    while (retry > 0) 
    {
        write_len = ::write(_fd_cp, data.str().c_str(), data_len); // $$ 真正寫 checkpoint 的地方, 把 data 組成 csv 的格式
        if (write_len <= 0) 
        {
            retry--;
        //    Error("%s, write check point fail (%s), stop transmitter\n", __func__, strerror(errno));
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("write check point fail: ") + strerror(errno), true);
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sleep a few seconds and retry (" + to_string(retry)+ " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                if (!reset_sync_chan()) 
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel fail: ") + strerror(errno) + ", retry " + to_string(retry) + " times left", true);
                    sleep(3);
                    continue;
                }
                else
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel success: new fd " + to_string(_fd_cp)), true);
                    continue;
                }
                
            }
            else 
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("unexpected error code: ") + strerror(errno), true);
                return false;
            }

        }
        else   
            break;
    } 

    
    /*
    if (write_len <= 0 && _stop_at_fail) {
        Error("%s, write check point fail (%s), stop transmitter\n", __func__, strerror(errno));
        transmitter::close_chan();
        exit(EXIT_CODE_STOP_AT_FAIL);
    }
    
    retry = 5;
    while (write_len <= 0 && --retry >= 0) {
        Error("%s, write check point fail (%s), retry after %d sec.\n",
            __func__, strerror(errno), try_after);
        sleep(try_after);
        ++try_after;
        if (!reset_sync_chan()) {
            Error("%s, reset sync channel fail: %s\n", __func__, strerror(errno));

            continue;
        }
        write_len = ::write(_fd_cp, data.str().c_str(), data_len);
    }
    */
    if (write_len < 0 || write_len != data_len) { // $$ 已經 retry 過了
    //    Error("%s, write check point fail: %s\n", __func__, strerror(errno));
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("write check point fail: ") + strerror(errno), true);
        return false;
    }

    // flush immediately and wait for finish
    // $$ syn_progress_mark 裡面的 P 代表 Progress, 所以下方的 read 得到的就是 progress 
    // ?? 但是傳 syn_progress_mark 幹嘛? 得到 progress 的用處? 是不是單純確認 checkpoint 寫成功了?
    // $$ Answer: syn_progress_mark 和 寫checkpoint 無關, 純粹是希望 9091 可以回報進度而已
    retry = 5;
    while (retry > 0 ) 
    {
        write_len = ::write(_fd_cp, syn_progress_mark, SYN_MARK_LEN);
        if(write_len < 0 || SYN_MARK_LEN != write_len)
        {
            retry--;
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("flush check point fail: ") + strerror(errno), true);
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sleep a few seconds and retry (" + to_string(retry)+ " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                if (!reset_sync_chan()) 
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel fail: ") + strerror(errno)+ ", retry " + to_string(retry) + " times left", true);
                    sleep(3);
                    continue;
                }
                else
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel success: new fd " + to_string(_fd_cp)), true);
                    continue;
                }
            }
            else 
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("unexpected error code: ") + strerror(errno), true);
                return false;
            }
        }
        else
            break;
    //    Error("%s: flush check point fail: %s\n", __func__, strerror(errno));
    }
    if(write_len < 0 || write_len != SYN_MARK_LEN)
        return false;

    uint64_t progress = 0;
    retry = 5;
    while ( retry > 0) 
    {
        if(read(_fd_cp, &progress, sizeof(uint64_t)) < 0 )
        {
            retry--;
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("Set checkpoint: ") + data.str().c_str() +" fail (" + strerror(errno) + ")(remaining retry: " + to_string(retry) + ")", true);
        //    Error("Set checkpoint: %s fail (%s)(remaining retry: %d)\n", data.str().c_str(), strerror(errno), retry);
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sleep a few seconds and retry (" + to_string(retry)+ " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                if (!reset_sync_chan()) 
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel fail: ") + strerror(errno) + ", retry " + to_string(retry) + " times left", true);
                    sleep(3);
                    continue;
                }
                else
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel success: new fd " + to_string(_fd_cp)), true);
                    continue;
                }
            }
            else 
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("unexpected error code: ") + strerror(errno), true);
                return false;
            }
        }
        else
            break;
    }
    if (retry <= 0) 
        return false;
    Debug("Set checkpoint: %s\n", data.str().c_str());
   
    // $$ 一開始 allow_backoff 初始化為true, 第一次寫checkpoint之後就設為false, 也就是除了手動重啟 streamer, 否則不允許 back off
//    allow_backoff = false;

    return true;
}

bool bo_transmitter::get_last_commitment(string& doc_id)
{
    // use another connection to fetch last row
    int fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO);
    if (-1 == fd_cp) {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("failed because ") + strerror(errno) + ", fd_cp: " + to_string(fd_cp), true);
        Debug("%s: open connection fail because of '%s'\n", __func__, strerror(errno));
        return false;
    }

    ostringstream sql;
    // BO doesn't support HAVING clause
    //sql << "SELECT doc, count(*) FROM " << _cp_name << " GROUP BY doc HAVING count(*) = " << _channels.size() << "ORDER BY mstime DESC limit 1";

    //// It's not necessary all tables have data in the last doc. Thus check cp_cnt == table_cnt is not necessary.
    //sql << "SELECT doc FROM " << _cp_name << " WHERE syn_group = '" << _group << "' ORDER BY mstime DESC LIMIT 1";

    // Each doc has to write commitment for all tables even no data change in some tables.
    // For strictly mode, it's necessary to check cp_cnt == table_cnt
    sql << "SELECT doc FROM (select doc, count(*) as cp_cnt, mstime from ";
    sql << _cp_name << " WHERE syn_group = '" << _group << "' group by doc) WHERE cp_cnt = " << get_main_channel_size() << " ORDER BY mstime DESC LIMIT 1";
    bool ret = bo_channel::exec_sql(fd_cp, "", sql.str(), true, doc_id);

//    Write_ConnectionLog(fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(fd_cp);

    if(!ret)
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("failed, because of ") + strerror(errno) + ", sql: '" + sql.str() + "'", true);
        return false;
    }
    Debug("bo_transmitter::%s: last commitment is '%s'\n", __func__, doc_id.c_str());

    return true;
}

bool bo_transmitter::get_checkpoint(string table, string doc_id, string& ws, ssize_t& count)
{
    int fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO);
    if (-1 == fd_cp)
        return false;

    bool ret;
    string result;
    ostringstream sql;
    sql << "SELECT ws, total FROM " << _cp_name << " WHERE syn_group='" << _group << "' AND table='" << table << "' AND doc='" << doc_id << "'";
    ret = bo_channel::exec_sql(fd_cp, "", sql.str(), true, result);

//    Write_ConnectionLog(fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(fd_cp);


    if (result.empty()) { // The table may not have data in the doc.
        ostringstream sql_last;

        // $$ 透過 doc_id找不到, 就拿最新的那筆
        sql_last << "SELECT ws, total FROM " << _cp_name << " WHERE syn_group='" << _group << "' AND table='" << table << "' ORDER BY mstime DESC LIMIT 1";

        fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO);
        if (-1 == fd_cp)
        {
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("failed, because of ") + strerror(errno) + ", fd_cp: "+ to_string(fd_cp), true);
            return false;
        }

        ret = bo_channel::exec_sql(fd_cp, "", sql_last.str(), true, result);

    //    Write_ConnectionLog(fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(fd_cp);

        if(!ret)
        {
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("failed, because of ") + strerror(errno) + ", sql: '" + sql_last.str() + "'", true);
            return false;
        }
    }
    
    count = 0;
    ws = _workspace;
    if (!result.empty()) {
        string::size_type pos = result.find_first_of(',');
        if (pos != string::npos) { // $$ 找到 ','
            ws = result.substr(0, pos);
            count = stoll(result.substr(pos+1));

            if(_workspace.compare(ws) != 0) // $$ not equal
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "workspace: " + _workspace + " and " + ws +" is not equal", true);
            }
        }
    } 
    else {
        cout<<"result is empty"<<endl;
    }
    /*
    count 是從 checkpoint table 拿到的值
    $$ health check for checkpoint

    $$ count 為 0 的可能性:
        1. 超過7天都沒收資料, checkpoint因為是sliding table所以被清空了, result.empty()為true, count 為 0
        2. 全新的streamer, checkpoint裡一筆資料都沒有, result.empty()為true, count 為 0
    採取做法:
        1. 保留原本 count == 0 && !_allow_cleanup 的判斷: 當checkpoint為0 且_allow_cleanup為false時, 不允許重新收資料
        2. 此時實際table算出來的值比checkpoint還要少時: 此get_checkpoint()在back off 時被called, 正常來說, table裡存的筆數只會筆checkpoint裡的多
        3. 當table實際算出來的筆數比checkpoint大非常多(超過一個batch_size), ex: 實際有 兩百萬筆, 但checkpoint只有 10筆


        ?? 隱憂: CK 說有時候在trim checkpoint的時候會多trim一筆, 有可能會造成算checkpoint時以為還剩一筆checkpoint, 但實際上什麼都沒有
        這是真的嗎?  有發生過嗎?
    */

    if(count == 0 && !_allow_cleanup)
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "health check: count is 0 and allow_cleanup is false", true);
        return false;
    }
    else
    {
        size_t sql_total_count;
        bool success;
        string success_str;
        success = get_total_count_by_sql(ws, table, sql_total_count);
        if(success)
            success_str = "true";
        else
            success_str = "false";
        
        
        int count_difference = int(sql_total_count) - int(count);
        
        /*
            $$  sql_total_count < checkpoint 的值, 不正常
            $$  因為就算 data 的 table 要搬資料導致 sql_total_count == 0, checkpoint 的值也要改成 0 才對
            $$  保持不動的是 checkpoint 的 doc id, 才能和 kafka 一致
        */    
        if(count_difference < 0) 
        {
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "health check " + success_str + ": sql_total_count < check_count:" + to_string(sql_total_count) + " < " + to_string(count), true);
            return false;
        }
            
        // $$ health check: 檢查 count(checkpoint的total) 和 total_count(實際去算table) 的筆數差多少, 是否正常
        // $$ 理論上 total_count 會比 count 大
        string batch_size;
        if (get_param_value("batch_size", batch_size))
            if(count_difference > stoi(batch_size)) // $$ 差距比一個 batch_size還大
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "health check " + success_str + ": sql_total_count - check_count:" + to_string(sql_total_count) + " - " + to_string(count) + " is greater than batch_size " + batch_size, true);
                return false;
            }
    }

    Debug("%s: check point for '%s' with doc '%s' and total count %ld in workspace '%s'\n",
        __func__, table.c_str(), doc_id.c_str(), count, ws.c_str());

    return true;
}

bool bo_transmitter::backoff(string& doc_id)
{
//    if(!allow_backoff) 
//    {
//        Error("backoff::%s: wants to backoff, but allow_backoff is false.\n", __func__);
//        return false;
//    }

    int fd_chan = -1;
    ssize_t shrink = -1;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        bo_channel* bo_chan = (bo_channel*)it->second;
        string ws;

        if (bo_chan->is_replica() && _replica.no_sync)
            continue;

        // 1. stop all channels' connection
        bo_chan->close_chan(true);

        if (!doc_id.empty()) {
            // 2. get total count of the check point
            if (!get_checkpoint(bo_chan->get_name(), doc_id, ws, shrink))
                return false;
            
        }
        else {
            shrink = 0; // no committed doc, shrink to 0;
            ws = _workspace;
        }

        if (shrink == 0 && !_allow_cleanup) {
        //    Error("bo_transmitter::%s: clean up table '%s' is not allowed.\n", __func__, bo_chan->get_name().c_str());
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "_allow_cleanup is false, clean up table '" + bo_chan->get_name() + "' is not allowed", true);

            return false;
        }

        // 3. re-connect and shrink for each channel
        {
            // $$ 這是 table 的 fd, 所以開了不馬上關
            ostringstream tbl_name;
            if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO)))
                return false;

            bo_chan->set_fd(fd_chan);
            if (!bo_chan->is_dim_tbl())
                tbl_name << ws << ".";
            tbl_name << bo_chan->get_name();
        
            bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, shrink); 

            Error("bo_transmitter::%s: back off '%s' to doc '%s' at %ld\n",
                __func__, tbl_name.str().c_str(), doc_id.c_str(), shrink);
        }

        // if backoff doc is not current workspace, clean up all data in the current workspace
        if (!bo_chan->is_dim_tbl() && _workspace.compare(ws)) {
            ostringstream tbl_name;
            if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO)))
                return false;
            bo_chan->set_fd(fd_chan);
            tbl_name << _workspace << "." << bo_chan->get_name();
            bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, 0);
            Error("bo_transmitter::%s: back off '%s' to doc '%s' at 0\n",
                __func__, tbl_name.str().c_str(), doc_id.c_str());
        }
    }

    // 4. delete newer check point

    bool ret = false;
    string dummy;
    ostringstream delete_stmt;
    delete_stmt << "DELETE FROM " << _cp_name << " WHERE syn_group = '" << _group << "'";
    if (!doc_id.empty()) {
        ostringstream get_timestamp;
        string doc_time;
        if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO)))
            return false;
        get_timestamp << "SELECT mstime FROM " << _cp_name << " WHERE syn_group = '" << _group;
        get_timestamp  << "' AND doc = '" << doc_id << "' ORDER BY mstime DESC LIMIT 1";
        ret = bo_channel::exec_sql(fd_chan, "", get_timestamp.str(), true, doc_time);

    //    Write_ConnectionLog(fd_chan, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(fd_chan);
        if (!ret)
            return false;
        delete_stmt << " AND mstime > " << doc_time << "";
    }
    
    if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO)))
        return false;
    ret = bo_channel::exec_sql(fd_chan, "", delete_stmt.str(), false, dummy);

//    Write_ConnectionLog(fd_chan, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(fd_chan);
//    cout<<ret<<", delete_stmt: "<< delete_stmt.str()<<endl;
    return ret;
}

bool bo_transmitter::create_workspace()
{
    int retry = 5;
    int try_after = 1;
    bool ret = false;
    string dummy;
    while (!ret && --retry >= 0) {
        int fd = open_connection(0, SVC_BO);
        if (-1 == fd) {
            Error("%s, open connection for workspace '%s' fail (%s), retry after %d sec (remains %d times retry.)\n",
                __func__, _workspace.c_str(), strerror(errno), try_after, retry);
            sleep(try_after);
            continue;
        }

        ret = bo_channel::exec_sql(fd, _workspace, "CREATE WORKSPACE " + _workspace, false, dummy);

    //    Write_ConnectionLog(fd, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(fd);
        if (!ret) {
            Error("%s, create workspace '%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                __func__, _workspace.c_str(), errno, try_after, retry);
            sleep(try_after);
        }
    }
    return ret;
}

void bo_transmitter::reset_workspace(bool change_bo) // ?? 還沒看
{
    // backup previous workspace
    if (_workspace.length() > 0 && _bk_port > 0) 
    {
        int fd_bk = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BACKUP);
        if (fd_bk > 0) 
        {
            ostringstream bk_cmd;
            bk_cmd << "{\"cmd\":1,\"wait\":0,\"args\":[\"" << get_name() << "\",\"" << _workspace << "\"";
            if (change_bo)
                bk_cmd << ",\"1\"";
            bk_cmd << "]}";
            const string& bk_cmd_str = bk_cmd.str();
            if (write(fd_bk, bk_cmd_str.c_str(), bk_cmd_str.length()) <= 0)
                Error("bo_transmitter::%s, send backup command fail: %s\n", __func__, strerror(errno));

    //        Write_ConnectionLog(fd_bk, "close", string(__FILE__), __LINE__, string(__func__), "");
            close(fd_bk);
        }
        else
            Error("bo_transmitter::%s, connect to backup service fail: %s\n", __func__, strerror(errno));
    }
    if (change_bo) {
        // close check_point connection
        if (-1 != _fd_cp)
        {
        //    Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
            close(_fd_cp);
        }
        // send change bo request
        if (_bk_port > 0) {
            int fd_bk = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BACKUP);
            if (fd_bk > 0) 
            {
                const char* change_bo_cmd = "{\"cmd\":2,\"wait\":1}";
                if (write(fd_bk, change_bo_cmd, strlen(change_bo_cmd)) <= 0)
                    Error("bo_transmitter::%s, send change bo command fail: %s\n", __func__, strerror(errno));
                else {
                    char rsp_buf[1024] = {0};
                    ssize_t rsp_len = 0;
                    while ((rsp_len = read(fd_bk, rsp_buf, sizeof(rsp_buf) - 1)) < 0 && EAGAIN == errno)
                        ;
                    if (rsp_len < 0)
                        Error("bo_transmitter::%s, read bo change response fail: %s\n", __func__, strerror(errno));
                }
        //        Write_ConnectionLog(fd_bk, "close", string(__FILE__), __LINE__, string(__func__), "");
                close(fd_bk);
            }
            else
                Error("bo_transmitter::%s, connect to backup service fail: %s\n", __func__, strerror(errno));
        }
    }

    if (_fix_ws.empty()) {
        time_t now = time(&now);
        char time_buf[16] = {0};
        strftime(time_buf, sizeof(time_buf) - 1, "%Y%m%d", gmtime(&now));
        _workspace = string("WS") + time_buf;
    }
    else {
        _workspace = _fix_ws;
    }
    Debug("%s, current work space: %s\n", __func__, _workspace.c_str());
}

bool bo_transmitter::prepare_channels(bool create_cp)
{
    // create work space first
    if (!create_workspace())
        return false;

    int fd_chan = -1;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) 
    {
        bo_channel* bo_chan = (bo_channel*)it->second;
        if (bo_chan->is_replica()) 
        {
            bo_chan->set_fd(open_connection(0, SVC_REPLICA)); // $$ 在 create_table() 裡面關 fd
            if (!bo_chan->create_table(""))
                return false;
        }
        else 
        {
            int retry = 5;
            int try_after = 1;
            bool ret = false;
            while (!ret && --retry >= 0) 
            {
                // $$ 建立一個 connection 之後, 用該 fd 建了 table
                bo_chan->set_fd(open_connection(0, SVC_BO)); // $$ 在 create_table() 裡面關 fd
                if (!(ret = bo_chan->create_table(_workspace))) 
                {
                    bo_chan->close_chan(true);
                    Error("%s, create_table '%s.%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                        __func__, _workspace.c_str(), bo_chan->get_name().c_str(), errno, try_after, retry);
                    sleep(try_after);
                }
                
            }
            if (!ret)
                return false;
        }
        // $$ Leo: 不管前面做得成不成功, 在下面又用 bo_chan->set_fd(fd_chan); 覆蓋掉 fd 之前, 一定要把舊的關掉
        // $$       理論上經過 create_table() 之後, _fd 就應該被 close 了且為 -1 才對
    //    bo_chan->close_chan(true); 

        // prepare connection
        // 92
        if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO)))
            return false;

        // $$ 這個 fd_chan 明明被存進 bo_chan 裡, 但在哪裡被關掉? Answer: 不關
        ostringstream tbl_name;
        if (!bo_chan->is_dim_tbl())
            tbl_name << _workspace << ".";
        tbl_name << bo_chan->get_name();
    
        if (!bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, -1)) {
        //    Write_ConnectionLog(fd_chan, "close", string(__FILE__), __LINE__, string(__func__), "");
            close(fd_chan);
            return false;
        }
        /*
            $$ 看 log, 這邊設好的 fd會被關掉, 在哪關? 而且這理論上應該是每個table的connection, 不應該被關掉才對?
            $$ Answer: 
            $$      如果這個 prepare_channels() 是被 initialize() called 的話, 之後會在 bool bo_transmitter::backoff(string& doc_id) 又會被關掉然後重開
            $$      但如果是因為 start() expire 才 call 這個 prepare_channels() 的話, 就在這裡開啟, 之後就不關了
        */ 
        bo_chan->set_fd(fd_chan);
    }

    if (create_cp) {
        // create check point table
        if (_create_cp) 
        {

            string dummy;
            ostringstream sql;
            sql << "CREATE DEFAULT SLIDING TABLE " << _cp_name;
            sql << " ('syn_group' CHAR(30), 'ws' CHAR(30), 'table' CHAR(30), 'doc' STRING(255), 'progress' INT64, 'total' INT64, 'mstime' INT64, 'timestamp' DATETIME64, TIMEBOUND(";
            sql << _cp_retention;
            sql << "))";

            int retry = 5;
            int try_after = 1;
            bool ret = false;
            while (!ret && --retry >= 0) {


                _fd_cp = open_connection(0, SVC_BO); // 94

                
                if (-1 == _fd_cp) {
                    Error("%s, open connection for check point fail (%s), retry after %d sec (remains %d times retry.)\n",
                        __func__, strerror(errno), try_after, retry);
                    sleep(try_after);
                    continue;
                }
                ret = bo_channel::exec_sql(_fd_cp, "", sql.str(), false, dummy);

            //    Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
                close(_fd_cp);

                _fd_cp = -1;
                if (!ret) {
                    Error("%s, create table for check point fail (%d), retry after %d sec (remains %d times retry.)\n",
                        __func__, errno, try_after, retry);
                    sleep(try_after);
                }
            }
            if (!ret)
                return false;
            Debug("%s: Created table '%s' for checkpoint\n", __func__, _cp_name.c_str());
        }

        // prepare connection for check point
        // $$ 這是 checkpoint 的 fd, 所以開了不馬上關
        if (-1 == (_fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO)))
            return false;
        if (!bo_channel::pre_transmit(_fd_cp, _cp_name, _type, -1)) {
         //   Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
            close(_fd_cp);
            _fd_cp = -1;
            return false;
        }
    }
    return true;
}

bool bo_transmitter::flush_bo()
{
    if (!flush_bo(SVC_BO))
        return false;
    if (_replica_num > 0 && !_replica.no_sync)
        return flush_bo(SVC_REPLICA);
    return true;
}


bool bo_transmitter::flush_bo(sa_svc_type svc_type)
{
    
    char time_buf[64] = {0};
    time_t now = time(NULL);
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&now));
    Error("%s, %s, start at %s\n", __func__, svc_type == SVC_BO ? "main" : "replica", time_buf);

    if (!_no_suspend) {
        flush_cmd(svc_type); // $$ SVC_BO or SVC_REPLICA
        
    /*  
        string dummy;
        int fd = open_connection(0, svc_type);
        if (-1 == fd) {
            
            Error("%s, open connection fail before suspend cmd: %s\n", __func__, strerror(errno));
            return false;
        }
        bo_channel::exec_sql(fd, "", "SUSPEND", false, dummy);
        close(fd);

        // $$ what happend here?? SUSPEND之後又RESUME, 是為了等另一邊 ssize_t bo_channel::transmit(void* data, size_t size) 嗎?
        
        fd = open_connection(0, svc_type);
        if (-1 == fd) {
           
            Error("%s, open connection fail before resume cmd: %s\n", __func__, strerror(errno));
            return false;
        }
        bo_channel::exec_sql(fd, "", "RESUME", false, dummy);
        close(fd);
    */    
        
    }

    now = time(NULL);
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&now));
    Error("%s, %s, finished at %s\n", __func__, svc_type == SVC_BO ? "main" : "replica", time_buf);

    if (svc_type == SVC_BO && _commit_act)
        send_commit_action(); // $$ Edward: 這是新加坡樟宜機場的東西

    return true;
}

bool bo_transmitter::flush_cmd(sa_svc_type svc_type)
{
    string dummy;
    bool success = true;
    // $$ ========= flush tables ==============

    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) 
    {
        string table_name = (string)it->first;

        int flush_table_fd = open_connection(0, svc_type);
        string table_sql = "flush table " + table_name; //"select * from " + table_name;//"flush table " + table_name;

        if(!bo_channel::exec_sql(flush_table_fd, _workspace, table_sql, false, dummy))
        {
            success = false;
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), table_sql + "; failed, fd: ("+ to_string(flush_table_fd)+ "): " + strerror(errno), true);
        }

    //    Write_ConnectionLog(flush_table_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(flush_table_fd);
    }
    // $$ ========= flush checkpoint ==============

    string cp_sql = "flush table " + _cp_name;
//    cout<<cp_sql<<endl;
    int flush_checkpoint_fd = open_connection(0, svc_type);
    if(!bo_channel::exec_sql(flush_checkpoint_fd, "", cp_sql, false, dummy))
    {
        success = false;
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), cp_sql + "; failed, fd: ("+ to_string(flush_checkpoint_fd)+ "): " + strerror(errno), true);
    }
//    Write_ConnectionLog(flush_checkpoint_fd, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(flush_checkpoint_fd);
    return success;
}

bool bo_transmitter::commit(string& doc_id)
{
    // $$ commit() 和 flush_bo() 其中一個失敗就 return false;
    if (!transmitter::commit(doc_id) ||
        !flush_bo())
        return false;
    return true;
}

bool bo_transmitter::flush(string& chan_name, int infd)
{
    return flush(chan_name, infd, 0);
}

bool bo_transmitter::flush(string& chan_name, int infd, int offset)
{
    bool ret = transmitter::flush(chan_name, infd, offset);
    if (_replica_num) {
        string rep_chan = chan_name + "._replica_";
        transmitter::flush(rep_chan, infd, offset);
    }

    return ret;
}

bool bo_transmitter::reset_chan(channel* chan)
{
    bo_channel* bo_chan = (bo_channel*)chan;

    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reconnect because of ") + strerror(errno) + ", close original connection and start a new one", true);

    Error("%s, close original connection and start a new one.\n", __func__);
    bo_chan->close_chan(true);

    int fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, ((bo_channel*)chan)->is_replica() ? SVC_REPLICA : SVC_BO);
    if (-1 == fd_chan)
        return false;

    ostringstream tbl_name;
    if (!bo_chan->is_dim_tbl())
        tbl_name << _workspace << ".";
    tbl_name << bo_chan->get_name();
    if (!bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, -1)) {
    //    Write_ConnectionLog(fd_chan, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(fd_chan);
        return false;
    }
    bo_chan->set_fd(fd_chan);
    return true;
}

bool bo_transmitter::reset_sync_chan()
{
    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reconnect because of ") + strerror(errno) + ",  close original connection " + to_string(_fd_cp) + " and start a new one", true);

    if (-1 != _fd_cp)
    {
    //    Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd_cp);
    }
    // $$ 這是 checpoint 的 fd, 所以開了不馬上關
    if (-1 == (_fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO)))
        return false;
    if (!bo_channel::pre_transmit(_fd_cp, _cp_name, _type, -1)) {
    //    Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd_cp);
        _fd_cp = -1;
        return false;
    }
    return true;
}

void bo_transmitter::close_chan(channel* chan)
{
    bo_channel* bo_chan = (bo_channel*)chan;
    bo_chan->close_chan(true);
}



void bo_transmitter::send_commit_action()
{
    // $$ Edward: 這是新加坡樟宜機場的東西
    char http_req[1024] = {0};
    snprintf(http_req, sizeof(http_req) - 1,
        "POST /prediction/los?src=%s HTTP/1.1\r\nHost: bigobject.io\r\nContent-length: 0\r\n\r\n",
        _cmact.source.c_str());

    int fd = simple_connection(_cmact.host, _cmact.port);
    if (-1 == fd)
        return;

    ssize_t req_len = strlen(http_req);
    ssize_t send_len = 0, send_ret = 0;
    while (0 < (send_ret = write(fd, &http_req[send_len], req_len - send_len))) {
        send_len += send_ret;
        if (send_len >= req_len)
            break;
    }
    if (send_ret <= 0) {
        Error("%s, send commit action request fail: %s\n",  __func__, strerror(errno));
    }

//    Write_ConnectionLog(fd, "close", string(__FILE__), __LINE__, string(__func__), "");
    close(fd);
}
