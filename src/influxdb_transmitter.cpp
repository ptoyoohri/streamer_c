//#include <sys/types.h>
//#include <sys/sendfile.h>
//#include <sys/socket.h>
//#include <sys/stat.h>
//#include <arpa/inet.h>
//#include <netdb.h>
//#include <unistd.h>
//#include <fcntl.h>

#include <strings.h>
#include <string.h>
#include <errno.h>
#include <vector>
//#include <sstream>


//#include "bo_counter.h"
#include "go_json2csv.h"
#include "influxdb_transmitter.h"
// #include "bo_stream.h"

#include <iostream>

using namespace std;



#define KEY_TRAN_INFLUXDB_ADDR       "influxdb_addr"
#define KEY_TRAN_INFLUXDB_PORT       "influxdb_port"

#define KEY_TRAN_INFLUXDB_USERNAME   "influxdb_username"
#define KEY_TRAN_INFLUXDB_PASSWORD   "influxdb_password"

#define KEY_TRAN_INFLUXDB_DBNAME     "influxdb_dbname"
#define KEY_TRAN_INFLUXDB_MEASUREMENT    "influxdb_measurement"

#define KEY_TRAN_INFLUXDB_RECV_FROM_BEGINING    "receive_from_begining"
#define TRAN_BO_READ_TIMEOUT    10

//////////////////////////////////////////////////
// bo_transmitter
//////////////////////////////////////////////////
influxdb_transmitter::~influxdb_transmitter()
{
/*
    if (_fd_cp != -1) {
        Write_ConnectionLog(_fd_cp, "close", string(__FILE__), __LINE__, string(__func__), "");
        close(_fd_cp);
        _fd_cp = -1;
    }
    */
}

bool influxdb_transmitter::initialize()
{
//    allow_backoff = true;

    if (!get_param_value(KEY_TRAN_INFLUXDB_ADDR, _influxdb_addr)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_ADDR);
        cout<<"influxdb_addr is not set"<<endl;
        return false;
    }
    
    if (!get_param_value(KEY_TRAN_INFLUXDB_PORT, _influxdb_port)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_PORT);
        cout<<"influxdb_port is not set"<<endl;
        return false;
    } 
    
    
    if (!get_param_value(KEY_TRAN_INFLUXDB_USERNAME, _influxdb_username)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_USERNAME);
        cout<<"influxdb_username is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_PASSWORD, _influxdb_dbname)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_PASSWORD);
        cout<<"influxdb_dbname is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_DBNAME, _influxdb_dbname)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_DBNAME);
        cout<<"influxdb_dbname is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_MEASUREMENT, _influxdb_measurement)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_MEASUREMENT);
        cout<<"influxdb_measurement is not set"<<endl;
        return false;
    }

    _receive_from_begining = false;
    string receive_from_begining;
    if (get_param_value(KEY_TRAN_INFLUXDB_RECV_FROM_BEGINING, receive_from_begining)) {
        _receive_from_begining = receive_from_begining.compare("1") == 0;
    }
    

    return true;
}

bool influxdb_transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;
//    if (!transmitter::set_params(group, param))
//        return false;
    return initialize();
}


bool influxdb_transmitter::flush(string& chan, int infd)
{

    /*
        $$ Leo: 雖然我寫的 Golang 的 CsvToInfluxDB 可以一次讀一個陣列的 fd
            但為了保持 streamer c++ 的一致性, 就維持還是一次只傳一個 fd
            只要在傳入之前組成只有一個element的 vector 就好

        $$  Leo: 跟上述理由一樣, string chan 傳進來只是為了為了架構一致, 其實沒有用 
                influxdb 的 measurement 其實是從 config.json 讀的
                因為 config.json 裡的 table_setting 設定的 table name 規定上是要跟 config 裡的一樣
                所以不會造成問題

                是否以後要改成只從一邊讀就好?
    */
    if(infd < 0)
        return true;

//    if (influxdb_measurement != chan)
//    {
//        return false;   
//    }

    vector<int> files_fd; // ?? 反正只放一個 fd, vector<int> files_fd(1) <-- reserve 1 再 push_back(infd), 反而vector裡變成, [0, infd], 不是預想的直接將 infd 塞在幫它 reserve 好的空間裡
    files_fd.push_back(infd);
    
    int i = 0;
    int files_num = files_fd.size();

    
    vector<GoInt> input_files;
    input_files.reserve(files_num);
    
    // $$ files_num: 多少個 kafka tempfile
    // $$ (GoInt)files_fd[i]: 印出來像是 kafka暫存檔的fd, 也就是說 x 筆資料就有 x 個kafka的暫存檔
    for (i = 0; i < files_num; ++i)
    {
        input_files[i] = (GoInt)files_fd[i];
    }
    GoSlice input = {&input_files[0], files_num, files_num};

   
//===================================================
    CsvToInfluxDB_return influx_ret = CsvToInfluxDB(
        //    { (char*)_out_dir.c_str(), (GoInt)_out_dir.length() }
        
        // char table_name[] = "csv_Leo_1";
        // { (const char*)(table_name), (GoInt)(sizeof(table_name)/sizeof(char) - 1) }, 
        
        { (char*)(_influxdb_addr.c_str()), (GoInt)_influxdb_addr.length() }, // $$ influxdb IP address
        { (char*)(_influxdb_port.c_str()), (GoInt)_influxdb_port.length() }, // $$ influxdb port
        { (char*)(_influxdb_username.c_str()), (GoInt)_influxdb_username.length() }, // $$ influxdb username
        { (char*)(_influxdb_password.c_str()), (GoInt)_influxdb_password.length() }, // $$ influxdb password

        { (char*)(_influxdb_dbname.c_str()), (GoInt)_influxdb_dbname.length() }, // $$ dbname
        { (char*)(_influxdb_measurement.c_str()), (GoInt)_influxdb_measurement.length() }, // $$ measurement
        input   // $$ 所有 kafka tempfile 的 fd 
    ); 
//=================================================

    // ?? Leo: 要怎麼把 Go bool 轉成 C++ 可以用的格式
    // $$ 原本是要接 influx_ret.r0.data, 但是 Go傳回來的 GoUint8 不知道怎麼轉成 C++ 的 bool
    // $$ 所以改用 err_msg 來判斷
    // $$ 研究 GoSlice
//        GoUint8 *success_int = &( (GoUint8*)influx_ret.r0); 
    
//        bool influx_success = (bool)(success_int[0]);

    vector<string> err_msg;
    err_msg.reserve(files_num);

    for (i = 0; i < files_num; ++i) 
    {
        GoString* err_go = &((GoString*)influx_ret.r1.data)[i];
        err_msg.push_back(string(err_go->p, err_go->n));
        
    // $$ 這些 file 會在 json2csv_adapter::transform 被關閉
    //    Write_ConnectionLog(files_fd[i], "close", string(__FILE__), __LINE__, string(__func__), "");
    //    close(files_fd[i]);
    }

    string temp_error_string;
    vector<string>::const_iterator it_err = err_msg.begin();
    for (; it_err != err_msg.end(); ++it_err) 
    {
        if (!it_err->empty()) {
            if(temp_error_string.empty())
                temp_error_string = it_err->c_str();
            else
                temp_error_string = temp_error_string + "; " + it_err->c_str();
        //    Error("%s, transform json to csv fail: %s\n", __func__, it_err->c_str());
            
            continue;
        }
    }
    if(!temp_error_string.empty())
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), temp_error_string, true);
        return false;
    }
//=================================================
        
    return true;
}
