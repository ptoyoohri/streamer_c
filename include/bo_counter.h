#pragma once

#include <string>
#include "common_func.h"

using namespace std;

typedef enum
{
    BCOUNT_KAFKA = 0,
    BCOUNT_ADAPTER,
    BCOUNT_TRANS,
    BCOUNT_MAX
} basic_counter_t;

class bo_counter
{
    public:
        static bo_counter* get_counter()
        {return _counter ? _counter : (_counter = new bo_counter());}

        void set_counter(const string& addr, unsigned short port, int retention);
        void start_counter();
        void stop_counter();

        void inc_val(basic_counter_t counter)
        {++_basic_val[counter];}
        void add_val(const string& type, const string& name, int val, time_t ts)
        {/*TODO*/}

        string get_counter_url()
        {return _url;}
        int get_retention()
        {return _retention;}
    protected:
        bo_counter();
        virtual ~bo_counter(){}

    private:
        bool _is_running;
        uint64_t _id;
        string _url;
        string _addr;
        unsigned short _port;
        int _retention;
        uint64_t _basic_val[BCOUNT_MAX * 2];

        static bo_counter* _counter;

        void submit();
        void send_counter(const string& type, const string& content);
};