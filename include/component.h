#pragma once

#include <sys/time.h>

#include <string>
#include <map>

#include "common_func.h"

#define SECT_CORD   "coordinator"
#define SECT_RECV   "receiver"
#define SECT_ADPT   "adapter"
#define SECT_TRAN   "transmitter"

#ifdef BO_STREAM_AGENT_DEBUG
#define Debug  printf
#else
#define Debug(...)
#endif
#define Error  printf

using namespace std;

class component
{
    public:
        component() : _group("") {}
        virtual ~component() {}

        virtual bool set_params(string& group, map<string, string>& param);

        bool get_param_value(string key, string& val);
    protected:
        string _group;
        map<string, string>     _params;
};

class commitment
{
    public:
        commitment() :
        _table(""), _doc_id(""), _total(0), _success(0), _timestamp({0, 0})
        {}
        commitment(string table, string doc, size_t total, size_t success) :
        _table(table), _doc_id(doc), _total(total), _success(success)
        {gettimeofday(&_timestamp, NULL);}
        virtual ~commitment() {}

        const string get_table_name() const
        {return _table;}
        void set_table_name(string table)
        {_table = table;}

        const string get_doc_id() const
        {return _doc_id;}
        void set_doc_id(string doc_id)
        {_doc_id = doc_id;}

        const size_t get_total_count() const
        {return _total;}
        void set_total_count(size_t total)
        {_total = total;}

        const size_t get_success_count() const
        {return _success;}
        void set_success_count(size_t success)
        {_success = success;}

        const struct timeval get_time_stamp() const
        {return _timestamp;}
        void set_time_stamp(struct timeval timestamp)
        {_timestamp = timestamp;}
    private:
        string  _table;
        string  _doc_id;
        size_t  _total;
        size_t  _success;
        struct timeval _timestamp;
};

class ini_conf
{
    public:
        ini_conf(string conf);
        virtual ~ini_conf(){}

        bool get_section(string section, map<string, string>& data);
        bool get_value(string section, string key, string& value);
    private:
        map<string, map<string, string> > _conf;
};
