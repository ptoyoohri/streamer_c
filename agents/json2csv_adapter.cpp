#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <unistd.h>
#include <dirent.h>

#include <sstream>



#include "bo_counter.h"
#include "go_json2csv.h"
#include "json2csv_adapter.h"

#include<iostream>

using namespace std;

#define KEY_ADAPTER_IS_FILE             "is_file"
#define KEY_ADAPTER_OUT_DIR             "out_dir"
#define KEY_ADAPTER_BATCH_SIZE          "batch_size"
#define KEY_ADAPTER_OUT_DIR_DEFAULT     "/tmp/bo_csv_out"
#define KEY_ADAPTER_KEEP_ALIVE_FILES    "keep_alive_files"
#define KEY_ADAPTER_CONF                "conf"

#define KEY_ADAPTER_FLUSH_TIME_LIMIT    "flush_time_limit"


bool json2csv_adapter::clear_cache()
{
    DIR* dir = opendir(_out_dir.c_str());
    if (!dir)
        return errno == ENOENT ? true : false;

    bool ret = true;
    struct dirent *ent = NULL;
    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        ostringstream path;
        path << _out_dir << "/" << ent->d_name;
        if (remove(path.str().c_str())) {
            Error("%s, remove %s fail: %s\n", __func__, path.str().c_str(), strerror(errno));
            ret = false;
            break;
        }
    }
    closedir(dir);
    return ret;
}

bool json2csv_adapter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string param_is_file;
    if (get_param_value(KEY_ADAPTER_IS_FILE, param_is_file) &&
        !param_is_file.empty())
        _is_file = true;

    struct stat outdir_stat = {0};
    if (!get_param_value(KEY_ADAPTER_OUT_DIR, _out_dir))
        _out_dir = KEY_ADAPTER_OUT_DIR_DEFAULT;
//    if (!clear_cache())
//        return false;
    if (0 != stat(_out_dir.c_str(), &outdir_stat)) {
        if (0 != mkdir(_out_dir.c_str(), S_IRWXU)) {
            Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
            return false;
        }
    }

    get_param_value(KEY_ADAPTER_CONF, _conf);
    if (!_conf.empty() && _conf.length() > 0)
        SetConfig({(char*)_conf.c_str(), (GoInt)_conf.length()});

    string batch_size;
    if (get_param_value(KEY_ADAPTER_BATCH_SIZE, batch_size))
        _batch_commit = stoi(batch_size);
    get_param_value(KEY_ADAPTER_KEEP_ALIVE_FILES, _keep_alive_files);

    string flush_time_limit;
    if(get_param_value(KEY_ADAPTER_FLUSH_TIME_LIMIT, flush_time_limit))
        _flush_time_limit = stoi(flush_time_limit);
    
    
    
    
    // set counter parameter for go program
    string counter_url = bo_counter::get_counter()->get_counter_url();
    int counter_retention = bo_counter::get_counter()->get_retention();
    if (!counter_url.empty())
        SetCounter({(char*)counter_url.c_str(), (GoInt)counter_url.length()}, (GoInt)counter_retention);
    return true;
}


bool json2csv_adapter::transform(vector<int>& files_fd, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    // $$ files_fd 為一堆 kafka tempfile 的 fd, 等一下用 fd 轉成 int 傳給 JsonToCSV()
    if (files_fd.empty()) 
    {
        if (_pending) 
        {
            // $$ means it is time out
            _pending = 0;
            
            check_flush_time("commit");

            return commit();
        }
        
        return true;
    }
    
    int i = 0;
    int data_len = files_fd.size();
    if (_is_file) {
        vector<GoInt> input_files;
        input_files.reserve(data_len);

        // $$ data_len: 為資料有多少筆
        // $$ (GoInt)files_fd[i]: 印出來像是 kafka暫存檔的fd
        for (i = 0; i < data_len; ++i)
        {

            input_files[i] = (GoInt)files_fd[i];
        }
        GoSlice input = {&input_files[0], data_len, data_len};

        JsonToCsv_return go_ret = JsonToCsv(  // $$ <-- Golang 的 parser
            input, // json input
            {(char*)_out_dir.c_str(), (GoInt)_out_dir.length()}); // output dir
        // $$ JsonToCsv_return go_ret裡有三個array: out_tbl, out_fd, err_str

        vector<string> err_msg;
        vector<vector<string> > tbl_name;
        vector<vector<int> > csv_fd;

        // $$ 預留 data_len 筆 kafka 資料的空間
        err_msg.reserve(data_len);
        tbl_name.reserve(data_len);
        csv_fd.reserve(data_len); 

        // we've to copy Go memory to avoid being GC.
        
    //    cout<<"data_len: "<< data_len<<endl;
        for (i = 0; i < data_len; ++i) 
        {
            GoString* err_go = &((GoString*)go_ret.r2.data)[i];
            err_msg.push_back(string(err_go->p, err_go->n));
            tbl_name.push_back(vector<string>());
            csv_fd.push_back(vector<int>());

            

            if (err_go->p) {
                //tbl_name.push_back(vector<string>());
                //csv_fd.push_back(vector<int>());
                
                close(files_fd[i]); 
                continue;
            }

            GoSlice* tbl_list_go = &((GoSlice*)go_ret.r0.data)[i]; 
            GoSlice* csv_list_go = &((GoSlice*)go_ret.r1.data)[i];
            int ret_len = tbl_list_go->len;
            if (ret_len == 0) {
                
                close(files_fd[i]);
                continue;
            }


            //***********************************************************************************************
            // ?? Leo: 這段是不是沒有用?, 把資料從 tbl_name 讀出來 塞進 tbl_list 和 csv_list 裡, 然後什麼也不做就進入下一次i的迴圈
            //  tbl_list 和 csv_list之後也沒用
            //  感覺只是為了 看 table name 有沒有大於 30 而已?

            vector<string>& tbl_list = tbl_name[i]; // 第 i 筆資料裡的table list, 丟給 tbl_list
            vector<int>& csv_list = csv_fd[i];
            tbl_list.reserve(ret_len); 
            csv_list.reserve(ret_len); 

            for (int j = 0; j < ret_len; ++j) 
            {
                GoString* tbl_name_go = &((GoString*)tbl_list_go->data)[j];
                GoInt* csv_file_go = &((GoInt*)csv_list_go->data)[j];
                if (tbl_name_go->n <= 0 || tbl_name_go->n > 30) // table name should be not greater than 30.
                { 
                    tbl_list.push_back("");
                    csv_list.push_back(-1);
                    Error("%s, invalid GoString (%ld)\n", __func__, (int64_t)tbl_name_go->n);
                }
                else 
                {
                    tbl_list.push_back(string( (const char*)tbl_name_go->p, tbl_name_go->n) );
                    csv_list.push_back((int)*csv_file_go);
                }
            }
            // ************************************************************************************************

            //tbl_name.push_back(tbl_list);
            //csv_fd.push_back(csv_list);
            
            close(files_fd[i]); // $$ close 在 kafka_receiver::read 裡建的 tempfile
            
        }


        // $$ Leo: it_err->c_str() is error message
        string temp_error_string;
        vector<string>::const_iterator it_err = err_msg.begin();
        vector<vector<string> >::const_iterator it_tbl_name = tbl_name.begin();
        vector<vector<int> >::const_iterator it_csv_fd = csv_fd.begin();

    //    cout<<"tbl_name.size(): "<<tbl_name.size()<<endl;
        for (; it_tbl_name != tbl_name.end(); ++it_err, ++it_tbl_name, ++it_csv_fd) 
        {
            if (!it_err->empty()) {
                if(temp_error_string.empty())
                    temp_error_string = it_err->c_str();
                else
                   temp_error_string = temp_error_string + "; " + it_err->c_str();
                Error("%s, transform json to csv fail: %s\n", __func__, it_err->c_str());
                
                continue;
            }
            if (it_tbl_name->empty() || it_csv_fd->empty())
                continue;

            vector<string>::const_iterator it_tbl = it_tbl_name->begin();
            vector<int>::const_iterator it_file = it_csv_fd->begin();


            for (; it_tbl != it_tbl_name->end(); ++it_tbl, ++it_file) 
            {
              
                if (*it_file < 0)
                    continue; 
                // $$ 看起來 channel name 和 table name 同名
                // $$ 把每個table都讀出來, for出來一個一個tansmit然後flush() (call 到 adapter::transmit(string chan, int infd) );
                if (!transmit(*it_tbl, *it_file)) 
                {
                    Error("%s, transmit file to chanel '%s' fail (fd %d): %s\n",
                            __func__, it_tbl->c_str(), *it_file, strerror(errno));       
                }     
                else
                {
                    // $$ 雖然有 GoInt 這個type, 但是GoString卻不能直接轉, 一定要透過這種方式. (https://www.itread01.com/content/1548387199.html)
                /*  
                    $$  $$$$$$ 超靈異事件:
                            如果另外宣告一個 string temp_tbl_name, 並且用他來轉換 GoString, 從此 (const char*)會被霸佔
                            之後只要宣告新的string, 或更改任何舊的 string, 就會把 tbl_name_influxdb 裡的值全部取代掉

                //    $$ string temp_tbl_name = *it_tbl;
                //    $$ tbl_name_influxdb.push_back( {(const char*)temp_tbl_name.c_str(), (GoInt)temp_tbl_name.length()} );
                 */
                //    cout<<"it_tbl: "<<*it_tbl<<", "<<temp_tbl_name<<", "<<(char*)temp_tbl_name.c_str()<<", "<<(GoInt)temp_tbl_name.length()<<endl;


                }
                
                close(*it_file); // $$ close 在 Golang Parser 裡面 WriteToFile() 建起來的 tempfile
             
            }


        }
        
        if(!temp_error_string.empty())
        {
            Write_Parse_ErrorLog(msg_info, temp_error_string, true);
        }
        
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }

    _pending += data_len;
    if (_pending >= _batch_commit) {

        _pending = 0;
        check_flush_time("commit");
        return commit();
    }
    else
    {
        /*
        $$
            Leo: 
                the only condition that will enter here, is that every data comes within 3 seconds, so it will never trigger timeout flush
                , but it will take too long to match _pending >= _batch_commit.
        */
        if(check_flush_time("compare") > _flush_time_limit) // $$ default is 300 sec
        {
            _pending = 0;
            check_flush_time("commit");
            return commit();
        }
    
    }
    
    return true;
}
