#include <string>
#include <vector>

#include "receiver.h"
#include<iostream>

using namespace std;

#define KEY_RECV_BATCH_PROC   "batch_proc"

bool receiver::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string batch;
    if (get_param_value(KEY_RECV_BATCH_PROC, batch))
        _batch_process = stoi(batch);

    return true;
}

bool receiver::start(time_t expire)
{
    bool ret = false;
    int data_cnt = 0, fd = -1;
    vector<int> batch_fd;
    set_state(AST_START);

    while (is_running()) {
        string data;

        msg_info_struct *msg_info = new msg_info_struct; // $$ for error log

        // $$ 傳入並取得 kafka tempfile 的 fd, 然後存進 batch_fd 裡
        if (read_receiver(data, fd, msg_info) <= 0)  // $$ call 到 kafka_receiver::read_receiver
        {
            // $$ read_receiver 都 return 1, 所以好像根本不會進來
            if (!batch_fd.empty()) {

            //    *_adapt << batch_fd;
                _adapt->bitshift_commit(batch_fd, msg_info);
                batch_fd.clear();
            }
            
            // send empty data to force commit
           // *_adapt << batch_fd;
            _adapt->bitshift_commit(batch_fd, msg_info);
            data_cnt = 0;
            break;
        }

        if (-1 != fd) 
        {
            // $$ 把 kafka tempfile 的 fd 存進 batch_fd 裡, 
            batch_fd.push_back(fd);
        
            if (++data_cnt >= _batch_process)  // $$ 收的資料超過 batch_process 寫進cache
            {
            //    *_adapt << batch_fd;
                _adapt->bitshift_commit(batch_fd, msg_info);
                batch_fd.clear();
                data_cnt = 0;
            }
            
        } 
        else 
        { 
            // $$ regular flush (commit)  
            // empty data means read timeout, force commit all
            if (!batch_fd.empty()) 
            {
            //    *_adapt << batch_fd;
                // $$ 把一堆 kafka tempfile 的 fd 傳給 virtual bool transform()
                _adapt->bitshift_commit(batch_fd, msg_info);
                batch_fd.clear();
                data_cnt = 0;
            }
            // send empty data to force commit
        //    *_adapt << batch_fd;
            _adapt->bitshift_commit(batch_fd, msg_info);
        }

        if (is_expired(expire)) 
        {
            if (!batch_fd.empty()) {
            //    *_adapt << batch_fd;
                _adapt->bitshift_commit(batch_fd, msg_info);
                batch_fd.clear();
                data_cnt = 0;
            }
            // send empty data to force commit;
            // *_adapt << batch_fd;
            _adapt->bitshift_commit(batch_fd, msg_info);
            ret = true;
            break;
        }
        delete msg_info;
    }
    return ret;
}

void receiver::stop()
{
    set_state(AST_STOP);
}

bool receiver::is_expired(time_t expire)
{
    return expire < time(NULL);
}