
#include <string.h>
#include <fstream>
#include <iostream>
#include "common_func.h"
#include <unistd.h>
#include <sys/time.h>

using namespace std;



void Write_Parse_ErrorLog(msg_info_struct *msg_info, string error_message, bool print_on_screen)
{
    string filename_ending_with = "_Parse_ErrorLog";
    string temp_msg = msg_info->topic + "\t" + to_string(msg_info->partition) + "\t" + to_string(msg_info->offset) +"\t"
            + error_message;

    Write_Log(filename_ending_with, temp_msg, print_on_screen);
    
}
void Write_System_ErrorLog(string file_name, int line, string function_name, string error_message, bool print_on_screen)
{
    string filename_ending_with = "_System_ErrorLog";
    error_message =  function_name + "()" 
        + "\t" + file_name 
        + "\t" + to_string(line)
        + "\t" + error_message;
    Write_Log(filename_ending_with, error_message, print_on_screen);
}



void Write_StressTestingLog(string message, bool print_on_screen)
{
    Write_Log("_StressTestingLog", message, print_on_screen);  
}


void Write_Log(string filename_ending_with, string log_message, bool print_on_screen)
{
    fstream fp;
    string file_name = get_current_date("date") + filename_ending_with + ".txt";

    string temp_string = get_current_date("date_time") + "\t" + get_current_date("unix_mili") + "\t" + log_message + "\r\n";
    if(print_on_screen)
        cout<<temp_string;

    fp.open(file_name, ios::out|ios::app);
    if(!fp)
        cout<<"open file "<<file_name<<" failed\n";
    else        
        fp<<temp_string;
    
    fp.close();
}

/*
void Write_ConnectionLog(int fd, string action, string file_name, int line, string function_name, string other_desc)
{
    string filename_ending_with = "_ConnectionLog";
    string message = action + " fd " + to_string(fd) 
        + ": " + function_name + "()" 
        + ": " + file_name 
        + ", line: " + to_string(line)
        + ": " + other_desc;
    Write_Log(filename_ending_with, message, false);
}
*/

string get_current_date(string format)
{
    string output;
    time_t rawtime;
    tm* timeinfo;
    char buffer [64] = {0};
    // 2019-07-08
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    if(format == "unix")
            output = to_string(rawtime);
    else if(format == "unix_mili")
    {
        struct timeval tp;
        gettimeofday(&tp, NULL);
        long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
        output =  to_string(ms);
    }
    else if(format == "date")
    {
        strftime(buffer,sizeof(buffer)-1,"%Y-%m-%d",timeinfo);
        output = string(buffer);
    }
    else if(format == "date_time")
    {
        strftime(buffer,sizeof(buffer)-1,"%Y-%m-%d %H:%M:%S",timeinfo);
        output = string(buffer);
    }
    
    return output;
}
