#pragma once

#include "receiver.h"
#include "common_func.h"

typedef enum {
    FRECV_SYNCTYPE_NORM,
    FRECV_SYNCTYPE_RECV_LAST,
    FRECV_SYNCTYPE_RECV,
    FRECV_SYNCTYPE_TRANS
} file_recv_sync_type;

class file_receiver : public receiver
{
    public:
        file_receiver() : _file_id(0), _path("."), _prefix(""), _interval(0), _stype(FRECV_SYNCTYPE_NORM)
        {}
        virtual ~file_receiver() {}

        virtual ssize_t read_receiver(string& data, int& fd, msg_info_struct *msg_info);

        virtual bool get_last_commitment(string& doc_id);

        virtual bool backoff(string& doc_id);

        virtual bool sync_strategy(string& doc1, string& doc2, string& out);

        virtual bool set_params(string& group, map<string, string>& param);
    private:
        int             _file_id;
        string          _path;
        string          _prefix;
        unsigned int    _interval; // in second
        file_recv_sync_type     _stype;

};