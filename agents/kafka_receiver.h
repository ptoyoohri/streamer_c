#pragma once

#include <vector>

#include <rdkafka.h>
#include "receiver.h"
#include "common_func.h"

typedef enum {
    KRECV_SYNCTYPE_NORM,
    KRECV_SYNCTYPE_RECV_LAST,
    KRECV_SYNCTYPE_RECV,
    KRECV_SYNCTYPE_TRANS
} kafka_recv_sync_type;




class kafka_receiver : public receiver
{
    public:
        kafka_receiver() :
        _broker("127.0.0.1:9092"), _kafka_group("bigobject.stream.agent"),
        _out2file(""), _read_timeout(1000), _batch_read(1), _stype(KRECV_SYNCTYPE_NORM),
		_kafka(NULL), _kafka_conf(NULL), _msg_tailing(false), _msg_heading(false), _msg_appending(false)
        {}
        virtual ~kafka_receiver();

        virtual ssize_t read_receiver(string& data, int& fd, msg_info_struct *msg_info);

        virtual bool get_last_commitment(string& doc_id);

        virtual bool backoff(string& doc_id);

        virtual bool sync_strategy(string& rdoc, string& tdoc, string& out);

        virtual bool set_params(string& group, map<string, string>& param);
    private:
        string      _broker;
        string      _kafka_group;
        string      _out2file;
		int			_read_timeout;
        int         _batch_read;
        kafka_recv_sync_type _stype;

        rd_kafka_t*         _kafka;
        rd_kafka_conf_t*    _kafka_conf;
        bool                _msg_tailing;
        bool                _msg_heading;
        bool                _msg_appending;

        vector<string>              _topics;

        // $$ 存每個 topic 和 partition 的 offset 
        // $$ _offsets[topic][partition] = offset
        // $$ _offsets 在 read_receiver 會一直更新
        vector<vector<ssize_t> >    _offsets; 
        

        bool set_partition_offset();
        bool get_partition_offset(vector<vector<ssize_t> >& part_offset);
        bool get_partition_offset_from_docid(const string& doc_id, vector<vector<ssize_t> >& part_offset);

        int find_topic_index(const char* topic);

        static void rebalance_cb(
            rd_kafka_t *kafka,
            rd_kafka_resp_err_t err,
            rd_kafka_topic_partition_list_t *partitions,
            void *arg);
};