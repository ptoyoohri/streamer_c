#include <iostream>
#include <string>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "kafka_receiver.h"
#include "json2csv_adapter.h"
#include "bo_transmitter.h"
#include "influxdb_transmitter.h"
#include "coordinator.h"


int main(int argc, char** argv)
{
    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "streamer start by shell script !!! ", true);
    int ret = 0;

    coordinator     *agent = NULL;    
    kafka_receiver      *kafka_recv = NULL;
    adapter             *adapter_ptr = NULL;
    transmitter         *transmitter_ptr = NULL;
    
//    json2csv_adapter    *j2c_adapt = NULL;
    
    influxdb_transmitter    *influxdb_trans = NULL;

//    csv2influxdb_adapter *c2i_adapt;


    if (argc < 2) {
        cout << "Usage: " << argv[0] << " <config file>" << endl;
        return -1;
    }

    // disable sigpipe
    // sine we use blocking mode and doesn't receive data from bo when sendfile,
    // we've no chance to aware of peer closed, thus we've to handle sigpipe at
    // sendfile. Instead of handle signal, we prefer to handle errno = EPIPE.
    if (SIG_ERR == signal(SIGPIPE, SIG_IGN))
        Error("%s, ignore SIGPIPE failed: %s\n", __func__, strerror(errno));
    //=======================================================================
    
    int retry = 30;
    int sleep_seconds = 30;
    do
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "streamer start by while loop (" + to_string(retry) + " times left)", true);

        ret = 0;
    
        agent = new coordinator(argv[1]);
        kafka_recv = new kafka_receiver;
        adapter_ptr = new json2csv_adapter;
    
        agent->set_receiver(kafka_recv);
        
    //   for( int i=0; i<argc; i++)
    //        cout<<i<<": "<<argv[i]<<endl;

        
        if(argc > 2)
            agent->set_write_to(argv[2]); 
        
        if (agent->get_write_to() != "influxdb")
            transmitter_ptr = new bo_transmitter;
        else
            transmitter_ptr = new influxdb_transmitter;

        agent->set_transmitter(transmitter_ptr);

        agent->set_adapter(adapter_ptr);

        if (!agent->initialize())
        {
            ret = -2;
            
            delete agent;
            delete kafka_recv;
            delete adapter_ptr;   
            delete transmitter_ptr;
            
            agent = NULL;
            kafka_recv = NULL;
            adapter_ptr = NULL;
            transmitter_ptr = NULL;

            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "initialize failed, sleep " + to_string(sleep_seconds) + " seconds and retry (" + to_string(retry)+ " times left)", true);
            
            sleep(sleep_seconds);
        }
        else
        {
            break; // success;
        }
        

    } while(--retry >= 0);

    if(retry >= 0 && ret >= 0)
    {
        /*
        if (agent->get_write_to() != "influxdb")
        {
            // synchronize both sides in advance
            if (!agent->sync()) 
            {
                ret = -3;
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sync failed", true);     
            }
        }
        */
        if (!agent->sync()) 
        {
            ret = -3;
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sync failed", true);     
        }
        if (ret >= 0)
            agent->run(); // $$ to coordinator::run(), 正常情況下, 進去後就不再出來了
    }

    delete agent;
    delete kafka_recv;
    delete adapter_ptr;   
    delete transmitter_ptr;

    agent = NULL;
    kafka_recv = NULL;
    adapter_ptr = NULL;
    transmitter_ptr = NULL;


    exit(ret);
    return ret;

    //==================================================================================
    /*
    agent = new coordinator(argv[1]);
    agent->set_receiver(&kafka_recv);

//   for( int i=0; i<argc; i++)
//        cout<<i<<": "<<argv[i]<<endl;

    if(argc > 2)
        agent->set_write_to(argv[2]); 
    
    if (agent->get_write_to() != "influxdb")
    {
        adapt = &j2c_adapt;    
        agent->set_transmitter(&bo_trans); // $$ 如果是influxdb根本不需要 BO

    }
    else
    {  
        adapt = &c2i_adapt;
    }

    agent->set_adapter(adapt);

//    agent->set_adapter(&c2i_adapt); 
    do {
        
        
        // initialize all parameters and connect each other
        if (!agent->initialize()) {
            ret = 101;// ret = -2;
            break;
        }

        if (agent->get_write_to() != "influxdb")
        {
            // synchronize both sides in advance
            if (!agent->sync()) {
                ret = -3;
                break;
            }
        }

        agent->run(); // $$ to coordinator::run(), 正常情況下, 進去後就不再出來了
    } while (0); 

    delete agent;
    exit(ret);
    return ret;
    */
}
