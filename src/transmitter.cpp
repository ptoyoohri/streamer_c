#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sstream>

#include "transmitter.h"
#include <iostream>

using namespace std;

#define KEY_TRAN_TABS       "table_num"
#define KEY_TRAN_TABNAME    "table_name_"
#define KEY_TRAN_TABBUF     "table_buf_"

#define KEY_TRAN_STOP_AT_FAIL   "stop_at_fail"

//////////////////////////////////////////////////
// channel
//////////////////////////////////////////////////
channel::channel(string name, size_t cap) :
    _name(name), _capacity(cap), _size(cap), _head(0), _tail(0),
    _data(NULL), _progress(0), _is_replica(false)
{
    if (0 == cap) // probably use input file instead.
        return;

    _data = (char*)mmap(NULL, _capacity, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (_data == MAP_FAILED)
        _data = NULL;
}

channel::~channel()
{
    if (_data && _capacity) {
        munmap(_data, _capacity);
        _data = NULL;
    }
}

bool channel::flush()
{
    ssize_t ret = 0;
    size_t size = 0;
    void* data = NULL;
    
    while (NULL != (data = get_data(size))) {
        ret = transmit(data, size);
        if (ret <= 0)
            break;
        release(ret);
    }
    return ret > 0 || errno == EAGAIN;
}

bool channel::flush(int infd)
{
    return transmit(infd) > 0 || errno == EAGAIN;
}

bool channel::flush(int infd, int offset)
{
    return transmit(infd, offset) > 0 || errno == EAGAIN; // $$ 通到 bo_channel::transmit(int infd, int offset)
}

//////////////////////////////////////////////////
// transmitter
//////////////////////////////////////////////////
transmitter::~transmitter()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        delete it->second;
    _channels.clear();
}

bool transmitter::flush(string& chan_name)
{
    channel* chan = get_chan(chan_name);
    if (!chan)
        return false;

    return flush(chan);
}

bool transmitter::flush(channel* chan)
{
    return chan->flush();
}

bool transmitter::flush(string& chan_name, int infd)
{
    return flush(chan_name, infd, 0);
}

bool transmitter::flush(string& chan_name, int infd, int offset)
{
    channel* chan = get_chan(chan_name);
    if (!chan)
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "just skip the channel: chan_name: " + chan_name, true);
        return true;
    }
    return flush(chan, infd, offset);
}

bool transmitter::flush(channel* chan, int infd)
{
    return flush(chan, infd, 0);
}

bool transmitter::flush(channel* chan, int infd, int offset)
{
    errno = 0;
    int retry = 5;
    bool ret;

    size_t total_count;

    
    static bool first_time = true;
    if(first_time)
    {
        first_time = false;
        chan->get_total_count(total_count);
        chan->StressTesting(total_count);
    }
    

    while (retry > 0) 
    {
        ret = chan->flush(infd, offset); // $$ 真正寫資料的 flush
        if (!ret)
        {
            retry--;
        //    Error("%s, write check point fail (%s), stop transmitter\n", __func__, strerror(errno));
            Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__)
                ,  "'"  + chan->get_name()
                + "' (fd " + to_string(chan->get_fd()) + ", offset " + to_string(offset) + ") failed: (" + strerror(errno) + ") "
                + " retry " + to_string(retry) + " time(s) left", true);
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sleep a few seconds and retry (" + to_string(retry) + " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                if (!reset_chan(chan)) 
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset sync channel fail: ") + strerror(errno) + ", retry " + to_string(retry) + " times left", true);
                    sleep(3);
                    continue;
                }
                else
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset chan success: new fd ") + to_string(chan->get_fd() ), true);
                    continue;
                }
            }
            else 
            {
                 Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("unexpected error code: ") + strerror(errno), true);
                return false;
            }
        }
        else   
        {
            /////////////////////////////////////////////////
        //    size_t total_count;
            chan->get_total_count(total_count);
            chan->StressTesting(total_count);
            /////////////////////////////////////////////////
            break;
        }
    } 
    
    return ret;
}

bool transmitter::flushall()
{
    bool ret = true;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        ret &= flush(it->second);
    return ret;
}

void transmitter::close_chan()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        close_chan(it->second);
}


bool transmitter::commit(string& doc_id)
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        if (!commit(it->second, doc_id))
            return false;
    }
    return true;
}

bool transmitter::commit(string& chan_name, string& doc_id)
{
    channel* chan = get_chan(chan_name);
    if (!chan)
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "get_chan " + chan_name + " fail: " + strerror(errno), true);
        return false;
    }
        
    return commit(chan, doc_id);
}

bool transmitter::commit(channel* chan, string& doc_id)
{
    if (chan->is_replica() && !chan->is_sync_replica())
        return true; // set check_point on master only

    int retry = 5;
    int ret = flush(chan);
    while (!chan->is_empty()) {
        usleep(1000); // sleep 1 ms
        flush(chan);
        retry--;
    }
    if (!chan->is_empty())
    {
        Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("channel is empty: ")  + strerror(errno), true);
        return false;
    }
    // $$ flush 完之後 用同樣的chan去 BO 拿 get_total_count()
    // $$ 看起來chan拿的是直接從checkpoint table裡的結果

    // $$ 加了這段 reset的機制後, get_total_count_by_sql() 理論上就用不到了
    size_t total_count;
    retry = 5;
    
    while(retry > 0)
    {
        if(!chan->get_total_count(total_count))
        {
            retry--;
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), "sleep a few seconds and retry (" + to_string(retry) + " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                if (!reset_chan(chan)) 
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset channel fail: ") + strerror(errno) + ", retry " + to_string(retry) + " times left", true);
                    sleep(3);
                    continue;
                }
                else
                {
                    Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("reset chan success: new fd ") + to_string(chan->get_fd()), true);
                    continue;
                }
            }
            else 
            {
                Write_System_ErrorLog(string(__FILE__), __LINE__, string(__func__), string("unexpected error code: ") + strerror(errno), true);
                return false;
            }
        }
        else
            break; // $$ success 
    }

    commitment cp(chan->get_name(), doc_id, total_count, chan->get_progress());
    
    if (!set_checkpoint(cp))
        return false;

    chan->reset_progress();
    return true;
}

bool transmitter::del_chan(string chan_name)
{
    map<string, channel*>::const_iterator it = _channels.find(chan_name);
    if (it == _channels.end())
        return false;
    delete it->second;
    _channels.erase(it);
    return true;
}

bool transmitter::reset_chan(string chan_name)
{
    channel* chan = get_chan(chan_name);
    if (!chan)
        return false;
    return reset_chan(chan);
}

bool transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string stop_at_fail;
    if (get_param_value(KEY_TRAN_STOP_AT_FAIL, stop_at_fail))
        _stop_at_fail = stop_at_fail.compare("1") == 0;

    string tab_num;
    int chan_num = 0, i = 0;
    if (!get_param_value(KEY_TRAN_TABS, tab_num) ||
        0 >= (chan_num = stoi(tab_num)))
        return false;

    string chan_name, chan_size;
    size_t size = 0;
    for (i = 1; i <= chan_num; ++i) {
        ostringstream name_key, buf_key;
        name_key << KEY_TRAN_TABNAME << i;
        buf_key << KEY_TRAN_TABBUF << i;
        if (!get_param_value(name_key.str(), chan_name) ||
            !get_param_value(buf_key.str(), chan_size))
            return false;

        switch (chan_size.at(chan_size.length() - 1)) {
            case 'k': case 'K':
                size = stoi(chan_size.substr(0, chan_size.length() - 1)) * 1024;
                break;
            case 'm': case 'M':
                size = stoi(chan_size.substr(0, chan_size.length() - 1)) * 1024 * 1024;
                break;
            default:
                size = stoi(chan_size.substr(0, chan_size.length()));
                break;
        }
        
        // $$ 從 config 裡讀 table name, 一個一個建出 fd 加到 _channels 裡
        if (!add_chan(chan_name, size))
        {
            return false;
        }
    }
    return true;
}

void transmitter::reset_progress()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        it->second->reset_progress();
}

void transmitter::reset_progress(string& chan_name)
{
    channel* chan = get_chan(chan_name);
    if (chan)
        chan->reset_progress();
}

void transmitter::inc_progress(string& chan_name, size_t progress)
{
    channel* chan = get_chan(chan_name);
    if (chan)
        chan->inc_progress(progress);
}

size_t transmitter::get_progress(string& chan_name)
{
    channel* chan = get_chan(chan_name);
    if (chan)
        chan->get_progress();
}

size_t transmitter::get_main_channel_size()
{
    size_t ret = 0;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        if (!(it->second->is_replica() && !it->second->is_sync_replica()))
            ++ret;
    }
    return ret;
}
